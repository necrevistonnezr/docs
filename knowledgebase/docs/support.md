# Support

## Forum

The <a href="https://forum.cloudron.io">Forum</a> is the best place to ask support
questions. The Forum makes reported issues SEO friendly and helps others (and you)
find resolutions to existing problems.
<br/>
<br/>
<center><a href="https://forum.cloudron.io" class="md-button" style="color: black;">Open Forum</a></center>

## Email

For sensitive support questions, please write to <a target="_blank" href="mailto:support@cloudron.io">support@cloudron.io</a>. Please send emails from your Cloudron.io account so that we can associate your request with your subscription. If you have multiple subscriptions, please put in your Cloudron ID as well. You can find the Cloudron ID in the `Settings` page of the dashboard as well as https://console.cloudron.io/#/cloudrons .

For sales related enquiries, please write to <a target="_blank" href="mailto:sales@cloudron.io">sales@cloudron.io</a>.

## Remote Support

To enable SSH access to your server for the Cloudron support team, use the `Enable SSH support access`
button in the `Support` view.

<center>
<img src="/img/remote-support.png" class="shadow" width="500px">
</center>

## SSH Keys

You can also add the following SSH keys manually. Depending on your setup, the keys below can be added
to the following file: `/home/cloudron-support/.ssh/authorized_keys`.

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGWS+930b8QdzbchGljt3KSljH9wRhYvht8srrtQHdzg support@cloudron.io
```

You can also run the following command:

```
cloudron-support --enable-ssh
```

!!! note "Keep the key to a single line"
    SSH key must be added as a single line. Make sure it doesn't break across multiple lines.

## Diagnostics

If the Cloudron dashboard is unreachable, it can be difficult to diagnose what is wrong.
You can SSH into your server and run the following script when contacting the Cloudron team.

This script collects some diagnostic information on the server and enables SSH support when given `--enable-ssh`

```
sudo cloudron-support [--enable-ssh]
```
