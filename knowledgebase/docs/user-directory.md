# User Directory

## Overview

Cloudron provides a central user directory that apps can use for authentication. This feature
allows users to use the same username & password for logging in to apps.

Cloudron is an [OIDC provider](#openid-connect) as well as a LDAP server. Cloudron App Policy
is to use OIDC integration whenever possible since this is more secure and support 2FA.

## Lock profile

Admins can disallow users from changing their email and full name by locking user profiles. To
lock the profile, simple uncheck the setting in the `User Directory` view.

<center>
<img src="/img/users-lock-profile.png" class="shadow" width="500px">
</center>

## Mandatory 2FA

Admins can require all users to set up two factor authentication by enabling the Mandatory 2FA setting.
To enable, use the setting in the `User Directory` view.

<center>
<img src="/img/users-mandatory-2fa.png" class="shadow" width="500px">
</center>

When enabled, all existing users without a 2FA setup are logged out immediately.

When users without 2FA attempt to login, they will be forced to setup 2FA:

<center>
<img src="/img/users-2fa-required.png" class="shadow" width="500px">
</center>

When the user clicks `Setup Two-Factor`, they go through the [2fa setup flow](/profile/#enabling-2fa):

<center>
<img src="/img/users-2fa-qrcode.png" class="shadow" width="500px">
</center>

## Directory Server

Cloudron can act as a LDAP server for apps hosted externally to Cloudron. External apps can then be configured
to list Cloudron users and allow users to authenticate with their Cloudron password.

You can enable the Directory Server from the `User Directory` view:

<center>
<img src="/img/users-directory-server.png" class="shadow" width="500px">
</center>

For security reasons, the LDAP server will only accept connections from specific white listed IPs and ranges.

### Configuring Clients

External apps can be configured to use the Directory Server as follows:

* Use the dashboard domain as the LDAP server hostname. Port 636 (TLS).
* LDAP server uses the same certificate as the dashboard domain.
* Set `cn=admin,ou=system,dc=cloudron` as the Bind DN.
* Use the secret listed in the above screenshot as the Bind Password.

### Troubleshooting

Users are listed under the `ou=users,dc=cloudron` search base. Groups are listed under the `ou=groups,dc=cloudron` search base.

The setup can be tested as follow:

```
$ ldapsearch  -x -b "ou=users,dc=cloudron" -D "cn=admin,ou=system,dc=cloudron" -W -H ldaps://my.example.com:636
Enter LDAP Password:
# extended LDIF
#
# LDAPv3
# base <ou=users,dc=cloudron> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# uid-0cfbd3d8-6547-4332-9415-dadfe8b78ac4, users, cloudron
dn: cn=uid-0cfbd3d8-6547-4332-9415-dadfe8b78ac4,ou=users,dc=cloudron
objectclass: user
objectclass: inetorgperson
objectclass: person
objectcategory: person
...
```

## External Directory

The External Directory connector allows users from an existing LDAP or Active Directory to authenticate with Cloudron.

When enabled, Cloudron will use profile information like Username, Display Name and Email from LDAP.

2FA behavior depends on the provider. When using the [Cloudron](#cloudron) provider, 2FA of the external directory is used.
When using other providers, users can setup 2FA locally.

The user's [role](/user-management/#roles) and [active state](/user-management/#disable-user) are local and not synced from LDAP.

### Providers

#### Cloudron

To use another Cloudron as the external LDAP directory, do the following:

* Enable `Directory Server` in the `User Directory` view of the other Cloudron. Be sure to whitelist this Cloudron
  and specify a secure secret.

* On this Cloudron, select `Cloudron` as the provider.

<center>
<img src="/img/user-directory-external-ldap-cloudron.png" class="shadow" width="500px">
</center>

!!! note "2FA Support"
    The Cloudron connector is the only one that supports 2FA. If the user has 2FA setup in the Cloudron LDAP Server, then 2FA is required to login. The [Mandatory 2FA](#mandatory-2fa) check is skipped for external users. The Mandatory 2FA flag can be enabled on the other Cloudron to enforce 2FA setup.

#### JumpCloud

The following screenshot shows the available configure options using a jumpcloud external LDAP directory:

* `Server URL`: `ldaps://ldap.jumpcloud.com:636`
* `Base DN`: `ou=users, o=3214565, dc=jumpcloud, dc=com`
* `Filter`: `(objectClass=inetorgperson)`
* `Bind DN`: `uid=ldap_admin,ou=Users,o=3214565,dc=jumpcloud,dc=com`
* `Bind password`: `admin password`
* `Username field`: `uid`

<center>
<img src="/img/user-directory-external-ldap-jumpcloud.png" class="shadow" width="500px">
</center>

#### Okta

To use the Okta integration, do the following:

* In Okta, enable the [LDAP interface](https://help.okta.com/en/prod/Content/Topics/Directory/LDAP_Using_the_LDAP_Interface.htm).
  You can do this from the `Directory Integrations` page.

* By default, Okta uses email as the default uid. Cloudron requires usernames for LDAP integration to work. If you already have a
  field in Okta that can provide usernames, provide that as the `username field`. If not, you can create a new field in the
  profile editor and set that.

* Cloudron configuration (replace 'org' below):
    * `Server URL`: `ldaps://<org>.ldap.okta.com`
    * `Base DN`: `ou=users, dc=<org>, dc=okta, dc=com`
    * `Filter`: `(objectClass=inetorgperson)`
    * `Bind DN`: `uid=<admin>, dc=<org>, dc=okta, dc=com`
    * `Bind password`: `admin password`
    * `Username field`: see above

<center>
<img src="/img/user-directory-external-ldap-okta.png" class="shadow" width="500px">
</center>

#### Disable

This disables External LDAP authentication. When disabled, Cloudron will switch all existing users to local.

<center>
<img src="/img/user-directory-external-ldap-disabled.png" class="shadow" width="500px">
</center>

### Sync

The local directory is synced with the external directory every 4 hours.

To trigger a manual sync, click the `Sync` button. Be sure to check the logs to see any conflicts.

<center>
<img src="/img/user-directory-external-ldap-sync.png" class="shadow" width="500px">
</center>

External users and groups have an icon in the User view:

<center>
<img src="/img/user-directory-external-user-icon.png" class="shadow" width="500px">
</center>

!!! warning "Users are not deleted"
    Currently, users removed from the external directory are not deleted from Cloudron during a sync.
    This is not a security issue because the user cannot authenticate anymore with the external directory.

#### Auto-create users

Use the `Automatically create users when they login to Cloudron` option to automatically create users locally on first login.

When not set, users are only Automatically created during [Sync](#sync).

#### Sync Groups

When `Sync Groups` is enabled, external groups will be created locally and users will be associated.

External Groups are readonly and cannot be edited. Therefore, local users cannot be added to external groups.

Local groups can still be created and they can have both local and external users as group members.

!!! warning "Groups are not deleted"
    Currently, groups removed from the external directory are not deleted from Cloudron during a sync.

#### Self signed certificate

Use the `Accept Self-signed certificate` option to accept any self-signed certificate from the LDAP server.

## OpenID Connect

[OpenID Connect 1.0](https://openid.net/connect/) is a simple identity layer on top of the OAuth 2.0 protocol.

Cloudron is an OIDC provider. The main advantages the OpenID integration offers over LDAP are:

* True single sign-on across apps. Once logged into the main dashboard, users can automatically login to apps.
* Manage app sessions from the Dashboard
* 2FA support across apps
* More secure since apps never see the user's password

Apps integrate automatically with the OIDC server using the [oidc addon](/packaging/addons/#oidc).

### Endpoints

| Name | URL |
|------|------ |
| Discovery URL 	| `https://my.cloudron.example/.well-known/openid-configuration` and `https://my.cloudron.example/openid/.well-known/openid-configuration`  |
| Issuer URL        | `https://my.cloudron.example/openid` |
| Auth Endpoint 	| `https://my.cloudron.example/openid/auth` |
| Token Endpoint 	| `https://my.cloudron.example/openid/token` |
| Keys Endpoint 	| `https://my.cloudron.example/openid/jwks` |
| Profile Endpoint 	| `https://my.cloudron.example/openid/me` |
| Logout URL 	    | `https://my.cloudron.example/openid/session/end` |

### Scopes and Claims

For most clients, it is recommend to add `openid` (mandatory), `profile` and `email` scope. Scopes are space separated.
This will ensure the app has access to the user profile claims.

Cloudron currently provides the following scopes and corresponding claims:

| Scope | Claim |
|------|------ |
| `profile` | `family_name`, `given_name`, `locale` (always `en-US`), `name`, `preferred_username` |
| `email` | `email`, `email_verified` |

On Cloudron, the `sub` property (the unique user identifier) is the **username**.

### OIDC Clients

OIDC clients can be managed in the `User Directory` view. To create a new client, provide the `clientid`, `clientsecret` and the callback URL.

<center>
<img src="/img/users-oidc-client-add.png" class="shadow" width="500px">
</center>

Then, in the client app, configure the provider as follows:

<center>
<img src="/img/users-oidc-client-config.png" class="shadow" width="500px">
</center>

