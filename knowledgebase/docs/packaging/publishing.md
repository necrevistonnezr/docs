# Publishing to Cloudron App Store

## Requirements

Publishing your app to the Cloudron App Store will help your users install it easily
on their server and keep it up-to-date.

Here are the rough steps involved in getting your app published:

* Before you start packaging, please leave a note in the [App Wishlist](https://forum.cloudron.io/category/5/app-wishlist) category
  of our forum. If a topic for your app does not exist, please create a new one. This will avoid any duplicate work since our community
  has already packaged apps and maybe you can use those as a starting point. You can also use this to guage interest before packaging.

* Package your app for Cloudron following the [tutorial](/custom-apps/tutorial/) and
  [cheat sheet](/packaging/cheat-sheet/). Feel free to ask any questions or help in the
  [App Packaging & Development](https://forum.cloudron.io/category/96/app-packaging-development) category
  of our forum. See the pinned topics in that category for answers to FAQs.

* Once packaged, please leave a note in your app's App Wishlist topic in our forum. Our community can provide you with early
  feedback and pre-release testing.

* At this point, Cloudron team will look into your package and get it ready for publishing. Please note that the
  Cloudron team will take over the packaging of the app from this point on as we have no mechanism for 3rd party
  authors to publish and update apps. As part of this process, we add automated tests to ensure the app installs, backs up,
  restores and updates properly.

## Licensing

We require app packages to have an Open Source license. MIT, GPL, BSD are popular choices but feel free to pick
whatever you are comfortable with. Please note that the license only applies to the packaging code and not to
your app. Your app can be Open Source or Commercial license.

The package will be maintained in our GitLab at [https://git.cloudron.io](https://git.cloudron.io). The original package authors will be given
commit permissions to the repository (and we greatly appreciate packagers who continue maintaining it!). To aid this process,
we recommend that the packaging source code is in a repository of it's own and not part of the app's code repository.

