# Email

## Overview

Cloudron has a built-in mail server that can send and receive email on behalf of users
and applications. By default, most of it's functionality is disabled and it only sends out
mails on behalf of apps (for example, password reset and notification emails).

When `Cloudron Email` is [enabled](#enable-email), it becomes a full-fleged mail server solution.
You can create mailboxes and assign a mailbox to one or more users or groups. Users can login
using Webmail/IMAP/POP3 to send and receive mail.

Features of this mail solution include:

* Multi-domain support
* Enable mailboxes for users and groups on a domain level
* Per-user and group mail aliases
* Mailbox sharing amongst users
* Group email addresses that forward email to it's members
* Email account sub-addressing by adding `+` tag qualifier
* Setup mail filters and vacation email using ManageSieve
* Catch all mailbox to receive mail sent to a non-existent mailbox
* Relay all outbound mails via SendGrid, Postmark, Mailgun, AWS SES or a Smart host
* Anti-spam. Users can train the spam filter by marking mails as spam. Built-in rDNS and
  zen spamhaus lookup. Admins can add custom spam rules for the entire server.
* Webmail. The [SnappyMail](https://cloudron.io/appstore.html?app=eu.snappymail.cloudronapp) and
  [Roundcube](https://cloudron.io/appstore.html?app=net.roundcube.cloudronapp) apps are already
  pre-configured to use Cloudron Email
* Completely automated DNS setup. MX, SPF, DKIM, DMARC are setup automatically
* Let's Encrypt integration for mail endpoints
* Domains and IP addresses blacklisting
* Server-side mail signatures (can be set per domain)
* [REST API](/api/) to add users and groups
* [Secure](/security/#email) out of the box
* Full text search using Solr
* Event Log
* Mail Queue management
* "All Mails" folder

<br/>

Email settings are located under the `Email` menu item.

<center>
<img src="/img/mail-settings.png" class="shadow" width="200px">
</center>

## Setup

### Incoming Email

By default, Cloudron's mail server only sends email on behalf of apps. To enable users to
**receive** email, turn on the incoming email option under `Email` -> Select domain -> `Incoming Email`.

<center>
<img src="/img/mail-enable.png" class="shadow" width="500px">
</center>

Clicking the `Enable` button will give you an option to automatically setup DNS records.

<center>
<img src="/img/mail-enable-confirm.png" class="shadow" width="500px">
</center>

When the `Setup Mail DNS records now` option is checked, Cloudron will automatically update
the `MX`, `SPF`, `DKIM`, `DMARC` DNS records of the domain. See the [DNS section](/#dns-records)
for more information.

!!! warning "Cloudflare hosted domains"
    Cloudflare does not proxy email (only http traffic). For this reason, please verify that Cloudflare
    proxying is disabled for the [mail server location](#server-location).

### Server Location

By default, the location of the email (IMAP & SMTP) server defaults to the Cloudron dashboard location i.e `my.domain.com`.
This can be changed in the `Mail` settings view.

Cloudron will automatically setup the required DNS records for all the domains when you change the mail server location.
Any installed webmail clients will be automatically re-configured as well. Be sure to adjust the settings of any
mobile and desktop mail clients accordingly.

<center>
<img src="/img/email-change-location.png" class="shadow" width="500px">
</center>

!!! note "PTR record"
    The [PTR record](#ptr-record) of the server's IP must be updated manually to the server location.

### Send Test Email

To send a test email, click the `Send Test Email` button.

<center>
<img src="/img/email-send-test-email-button.png" class="shadow" width="500px">
</center>

A dialog will popup where you can enter the email address to send the test email to:

<center>
<img src="/img/email-send-test-email.png" class="shadow" width="500px">
</center>

If you are not receiving emails, check the [server status](#server-status) and the [event log](#event-log).

### Firewall

Sending and receiving email requires TCP ports to be opened up on the server. Cloudron will automatically
manage opening up and closing the ports below in the server's firewall.

If you have a Cloud firewall in front of the server like EC2 security group, DO/Linode/Vultr Cloud Firewall,
the ports below have to opened up in them.

#### Inbound ports

The following ports are required for receiving mail.

| Port | Notes |
|------|------ |
| 25 (SMTP/TCP) | Required for receiving email. When not using Cloudron Email, this can be blocked. |
| 465 (SMTP/TCP) | Used for submitting email via TLS from mobile phone or desktop apps. When using only webmail or not using Cloudron Email, this port can be blocked. |
| 587 (SMTP/TCP) | Used for submitting email via STARTTLS from mobile phone or desktop apps. When using only webmail or not using Cloudron Email, this port can be blocked. |
| 993 (IMAP/TCP) | Used for accessing email from mobile phone or desktop apps. When using only webmail or not using Cloudron Email, this port can be blocked. |
| 4190 (Sieve/TCP) | Used for accessing email filters from mobile phone or desktop apps. When using only webmail or not using Cloudron Email, this port can be blocked. |

#### Outbound ports

The following ports are required for sending mail.

| Port | Notes |
|------|------ |
| 25 (SMTP/TCP) | Required for sending out emails. If outbound port 25 is blocked by your server provider, [setup an email relay](#relay-outbound-mails). You can check if outbound port 25 is blocked by sending yourself a [test email](#send-test-email) from the Cloudron. |

## Mailbox

### Add

Mailboxes can be created for Users and Groups on a per-domain level. To do so, simply create
them in the `Email` view.

<center>
<img src="/img/mail-add-mailbox.png" class="shadow" width="500px">
</center>

The `Mailbox Owner` dropdown can be used to select an existing user or group. The user can then access their
email using the new email and the Cloudron password using [SMTP](#smtp) and
[IMAP](#imap).

When a group is selected as the owner, any member of the group can access the mailbox with their password.

Mailboxes have the following naming restrictions:

* Only alphanumerals, dot and '-' are allowed
* Maximum length of 200 characters
* Names ending with `.app` are reserved by the platform for applications
* Names with `+` are not allowed since this conflicts with the [Subaddresses and tags](#subaddresses)
  feature.

### Remove

Use the delete button to delete a mailbox.

<center>
<img src="/img/email-remove-mailbox.png" class="shadow" width="500px">
</center>

After deletion, emails to this mailbox will bounce. If you have a catch-all address set, then emails
will get delivered to that mailbox.

!!! note "Deleting old emails"
    Deleting the mailbox does not remove old emails. You can remove the emails by removing the directory
    `/home/yellowtent/boxdata/mail/vmail/<mailbox@domain.com>`.

### Disable

A mailbox can be temporarily disabled by unchecking the `Mailbox is active` check box.

<center>
<img src="/img/mail-disable-mailbox.png" class="shadow" width="500px">
</center>

Once disabled, emails to this mailbox will bounce. If you have a catch-all address set, then emails
will get delivered to that mailbox.

### Export Mailboxes

Mailboxes can be imported as CSV or JSON.

<center>
<img src="/img/mail-mailboxes-export.png" class="shadow" width="500px">
</center>

The CSV has the following format:

```
name,domain,owner,owner type,space separate aliases,active
```

The JSON is an array of the following fields:

* name - name of the mailbox
* domain - domain of the mailbox
* owner - this is usually the username but could also be a group name or an app
* ownerType - `user`, `group` or `app`
* aliases - this is an array of objects containing the `name` and `domain` of the alias
* active - whether mailbox is enabled

### Import Mailboxes

Mailboxes can be imported as CSV or JSON.

<center>
<img src="/img/mail-mailboxes-import.png" class="shadow" width="500px">
</center>

The CSV has the following format:

```
name,domain,owner,owner type
```

The JSON is an array of the above fields.

## Mail aliases

Aliases are alternate addresses for the mailbox. Emails sent to an alias will be delivered to the mailbox.

Aliases can be on the same domain or another domain. Aliases can also contain the `*` wildcard to match
zero or more characters.

<center>
<img src="/img/email-alias.png" class="shadow" width="500px">
</center>

To send email with the alias as the "From" address, add them as an identity in your mail client.
Use the mailbox credentials for authentication when sending email using an identity.

!!! note "Authenticating with alias is not supported"
    It is not possible to login using the alias address.

## Mailing List

A Mailing list forwards emails to one or more email addresses. A list can be created in the `Email` view.

<center>
<img src="/img/mail-add-maillist.png" class="shadow" width="500px">
</center>

Use the 'Restrict posting to members only' option to allow only members to post to the list. When enabled,
non-members will get a bounce.

To support forwarding mails to external address, Cloudron implements [SRS](http://www.open-spf.org/SRS).
SRS translation is performed using the mailing list domain before forwarding the mail.

When delivering mails to a list, the mail server checks the `To` and `Cc` fields of the message and suppresses
duplicate mail delivery.

!!! note "No subscribe/unsubscribe feature"
    Cloudron does not support creating a mailing list (i.e) a list that allows members to
    subscribe/unsubscribe.

## Catch-all address

A Catch-all or wildcard mailbox is one that will "catch all" of the emails addressed
to non-existent addresses. You can forward such emails to one or more user mailboxes
in the Email section. Note that if you do not select any mailbox (the default), Cloudron
will send a bounce.

<center>
<img src="/img/email-catch-all-mailbox.png" width="500" class="shadow">
</center>

## Email Client Configuration

### IMAP

Use the following settings to receive email via IMAP:

  * Server Name - Use the [mail server location](#server-location) of your Cloudron (default: `my` subdomain of the primary domain)
  * Port - 993
  * Connection Security - TLS
  * Username/password - Use the email id as the username and the Cloudron account password

!!! note "Multi-domain setup credentials"
    Use the email id as the username to access different mailboxes. For example, if email is
    enabled on two domains `example1.com` and `example2.com`, then use `user@example1.com`
    to access the `example1.com` mailbox and use `user@example2.com` to access the `example2.com`
    mailbox. In both cases, use the Cloudron account password.

### SMTP

Use the following settings to send email via SMTP:

  * Server Name - Use the [mail server location](#server-location) of your Cloudron (default: `my` subdomain of the primary domain)
  * Port - 587
  * Connection Security - STARTTLS
  * Username/password - Use the email id as the username and the Cloudron account password

!!! note "Multi-domain setup credentials"
    Use the email id as the username to send email. For example, if email is
    enabled on two domains `example1.com` and `example2.com`, then use `user@example1.com`
    to send email as `example1.com` and use `user@example2.com` to send email as `example2.com`.
    In both cases, use the Cloudron account password.

### Sieve

Use the following settings to setup email filtering users via ManageSieve.

  * Server Name - Use the [mail server location](#server-location) of your Cloudron (default: `my` subdomain of the primary domain)
  * Port - 4190
  * Connection Security - STARTTLS
  * Username/password - Use the email id as the username and the Cloudron account password

!!! note "Multi-domain setup credentials"
    Use the email id as the username to access different mailboxes. For example, if email is
    enabled on two domains `example1.com` and `example2.com`, then use `user@example1.com`
    to access the `example1.com` mailbox and use `user@example2.com` to access the `example2.com`
    mailbox. In both cases, use the Cloudron account password.

### POP3

Use the following settings to receive email via POP3:

  * Server Name - Use the [mail server location](#server-location) of your Cloudron (default: `my` subdomain of the primary domain)
  * Port - 995
  * Connection Security - TLS
  * Username/password - Use the email id as the username and the Cloudron account password

POP3 access is disabled by default and must be enabled per-mailbox.

<center>
<img src="/img/mail-pop3.png" width=500 class="shadow">
</center>

## Subaddresses

Emails addressed to `<username>+tag@<domain>` i.e mail addresses with a plus symbol in the
username will be delivered to the `username` mailbox. You can use this feature to give out emails of the form
`username+kayak@<domain>`, `username+aws@<domain>` and so on and have them all delivered to your mailbox.

To send email with the subaddress as the "From" address, add it as an identity in your mail client.
Use the mailbox credentials for authentication when sending email using an identity.

## Relay outbound mails

By default, Cloudron's built-in mail server sends out email directly to recipients.
You can instead configure the Cloudron to hand all outgoing emails to a 'mail relay'
or a 'smart host' and have the relay deliver it to recipients. Such a setup is useful when the Cloudron
server does not have a good IP reputation for mail delivery or if server service provider
does not allow sending email via port 25 (which is the case with Google Cloud and Amazon EC2).

!!! note "Send from any email address on a domain"
    Cloudron uses the relay to send all outbound emails for the domain. For this reason, the relay
    must allow Cloudron to send emails as `<any-from-email>@mydomain.com`. This is usually
    called "Domain verified" identity. Using a relay that is able to send just a single address ("email address entity")
    will not work.

Cloudron can be configured to send outbound email via:

* [Amazon SES](#amazon-ses)
* Elastic Email
* Google
* Mailgun
* Mailjet
* Postmark
* Sendgrid
* [Office 365](#office-365)
* Sparkpost
* [External SMTP server](#smtp-server)

To setup a relay, enter the relay credentials in the Email section. Cloudron only supports relaying
via the STARTTLS mechanism (usually port 587).

<center>
<img src="/img/email-relay.png" width=500 class="shadow">
</center>

Community Guides:

* [SMTP Relay Configuration](/guides/community/smtp-relay-configuration.md)

### Amazon SES

To setup Cloudron to relay via Amazon SES:

* Add Domain Identity - Go to Amazon SES dashboard and add a new Domain Identity. Note that Email address Identity will not work
  because apps on Cloudron send emails with different email addresses. When verifying the domain, leave the `Provide DKIM authentication token` unchecked.

<center>
<img src="/img/email-relay-ses-identity.png" width=500 class="shadow">
</center>

* To complete domain verification, add the DNS keys (CNAME records) in the domain's DNS.

* Once domain is verified, click on `Account dashboard` in the left pane and scroll down to `Create SMTP credentials`.

<center>
<img src="/img/email-relay-ses-smtp-credentials.png" width=500 class="shadow">
</center>

  Follow through the wizard to create a new IAM user that has the following policy

```
        "Statement": [{  "Effect":"Allow",  "Action":"ses:SendRawEmail",  "Resource":"*"}]
```

* Setup the relay on the Cloudron under the Email section:

<center>
<img src="/img/email-relay-ses.png" width=500 class="shadow">
</center>

* Use the [Send Test Email](#send-test-email) button to verify emails are sent.

* If you do not receive the email, please verify that your AWS SES is not in sandbox mode. In this mode, new AWS
   accounts are only able to send mails to verified domains or the simulator. You can check this in the
   `Sending Statistics` page and looking for a note that looks like below:

<center>
<img src="/img/email-relay-ses-sandbox.png" width=500 class="shadow">
</center>

To remove sandbox, log a request to increase the sending limit to say 500 emails a day. Note that,
a [custom MAIL FROM domain](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/mail-from.html) must
be set for the DMARC alignment to succeed.

### Google

When using Google to relay mail, if you encounter an error message of the form `Invalid login` or
`Please log in via your web browser and then try again`, you must configure your Google account
to either use App passwords or enable less secure apps. See [Google Support](https://support.google.com/mail/answer/7126229?visit_id=1-636433350211034673-1786624518&rd=1#cantsignin) for more information.

### Office 365

To setup Office 365 as relay, add a connector under mail flow following the instructions under [Option 3](https://support.office.com/en-us/article/How-to-set-up-a-multifunction-device-or-application-to-send-email-using-Office-365-69f58e99-c550-4274-ad18-c805d654b4c4). Note that relaying via Office 365 requires port 25 to be open and requires a static IP.

### SMTP Server

Cloudron can relay via an external SMTP server with or without authentication. Use `External SMTP server`
option for relaying via a server with a username/password. For IP based authentication relays, use the
`External SMTP sever (No authentication)`.

## Forward all emails

To forward some or all emails to an external address, create a Sieve filter. Sieve
filters can be created using [SnappyMail](/apps/snappymail/),
[Roundcube](/apps/roundcube/) or any other client that supports
Manage Sieve.

<center>
<img src="/img/forward-all-emails-rainloop.png" class="shadow" width="500px">
</center>

To support forwarding mails to external address, Cloudron implements [SRS](http://www.open-spf.org/SRS).

## Marking Spam

The spam detection agent on the Cloudron requires training to identify spam.
To do this, simply move your junk mails to the pre-created folder named `Spam`.
Most mail clients have a Junk or Spam button which does this automatically.

If you marked a mail as Spam incorrectly, just move it out to the Inbox and
the server will unlearn accordingly.

The mail server is configured to act upon training only after seeing atleast
50 spam and 50 ham messages.

## DNSBL

The email server looks up the connecting IP address against real-time IP blocklists services known as DNSBL or RBL.
By default, Cloudron uses the `zen.spamhaus.org` service from the [Spamhaus project](https://www.spamhaus.org/).

The DNSBL servers can be configured in the Email view.

<center>
<img src="/img/mail-dnsbl.png" class="shadow" width="500px">
</center>


Some of the popular DNSBL services are listed below. Please be sure to check up the reliability and trustworthiness of the
services below before enabling them. [This discussion](https://forum.cloudron.io/topic/4677/is-there-a-way-to-add-in-more-dnsbl-rbl-sources) is worth a read.

* `bl.mailspike.net`
* `noptr.spamrats.com`
* `bl.0spam.org`
* `dnsbl.sorbs.net`
* `black.junkemailfilter.com`
* `all.spamrats.com`
* `zen.spamhaus.org` (default)

## Address blocklist

Use the spam filter configuration UI in the `Email` view to blacklist addresses
and domains. Matched addresses will end up in the user's Spam folder. `*` and `?`
glob patterns are supported. This is a global setting and applies for incoming
mail to all domains.

<center>
<img src="/img/mail-spam-blacklist.png" class="shadow" width="500px">
</center>

When an email matches the above addresses, it will have `USER_IN_BLACKLIST` field
set in the `X-Spam-Status` email header.

!!! note "Blocking IP Addresses"
    To block an IP address or an entire network, use the [Firewall](/networking/#firewall)
    configuration.

## Custom Spam Filtering Rules

Custom spam filter rules can be set in the spam filter configuration UI in the `Email`
view. Spam filtering rules are global and applies for incoming mail to all domains.

<center>
<img src="/img/mail-spam-rules.png" class="shadow" width="500px">
</center>

A simple Spam Assassin configuration rule to mark all emails containing the word 'discount'
in the subject looks like this:

```
header SUBJECT_HAS_DISCOUNT  Subject =~ /\bdiscount\b/i
score SUBJECT_HAS_DISCOUNT   100
describe SUBJECT_HAS_DISCOUNT    I hate email discounts
```

See this [guide](https://cwiki.apache.org/confluence/display/SPAMASSASSIN/WritingRules)
for writing custom rules.

!!! note "Spam Threshold"
    Emails above the spam score of 5.0 are considered spam and will have the `X-Spam-Flag` set to
    `YES` in the email header.

## Change FROM address of an app

By default, Cloudron allocates the `location.app@domain` mailbox for each installed app. When
an app sends an email, the FROM address is set to `location.app@domain.com`. The mailbox name
can be changed in the [configure dialog](/apps/#configuration) of the app.

<center>
<img src="/img/apps-mailbox-name.png" class="shadow" width="500px">
</center>

## Disable FROM address validation

By default, the Cloudron does not allow masquerading - one user cannot send email pretending
to be another user. To disable this, enable masquerading in the Email settings.

<center>
<img src="/img/email-masquerading.png" class="shadow" width="500px">
</center>

## Max mail size

The maximum size of emails that can be sent can be set using the Maximum Mail Size setting.

<center>
<img src="/img/email-max-size.png" class="shadow" width="500px">
</center>

## Storage quota

Storage limit can be set per mailbox by enabling Storage Quota.

<center>
<img src="/img/email-storage-quota.png" class="shadow" width="500px">
</center>

When storage limit is hit, the email sender will get a bounce message indicating that the receiving
mail box is full.

An email notification is placed in the mailbox when it nears 80% and 95% of it's storage quota.
Entries are created in the [event log](#event-log) as well.

## Vacation mail

An out of office / vacation mail message can be setup using Sieve filters. When using
SnappyMail, a vacation message can be set in `Settings` -> `Filters` -> `Add filter` -> `Vacation message` action.

<center>
<img src="/img/email-vacation-message-rainloop.png" class="shadow" width="500px">
</center>

## Signature

A disclaimer, confidentiality information or legalese can be appended to every outbound email via the  Email Signature setting.
This setting is per-domain.

<center>
<img src="/img/email-signature.png" class="shadow" width="500px">
</center>

## Event Log

Mail server activity can be monitored using the Eventlog UI.

<center>
<img src="/img/email-logs-button.png" class="shadow" width="500px">
</center>

Clicking the `Logs` button will open up the Eventlog.

<center>
<img src="/img/email-eventlog.png" class="shadow" width="500px">
</center>

Clicking the `Logs` button again in the Eventlog view will open up the raw logs. Raw logs are located in the disk at `/home/yellowtent/platformdata/logs/mail`. Up to 10MB of current logs are retained along side 1 rotated log. Logs older than 14 days are removed.

!!! note "Only available for superadmin"
    For security & privacy reasons, email event log is only viewable by superadmins (as they most likely have SSH access anyway).

## Mail Queue

Mail queue be monitored and managed using the Mail Queue UI.

<center>
<img src="/img/email-queue-button.png" class="shadow" width="500px">
</center>

Clicking the `Queue` button will open up the `Mail Queue`.

<center>
<img src="/img/email-queue.png" class="shadow" width="500px">
</center>

## Full Text Search

By default, every text search involves a [slow sequential search](https://doc.dovecot.org/configuration_manual/fts/#searching-in-dovecot) through all emails. With a small number of emails (< 5GB), the performance of search is usually
acceptable. If there are a large number of emails, the emails can be indexed to make search faster.

To enable the search index, enable Full Text Search (Solr) from the Email settings:

<center>
<img src="/img/email-fts-enable.png" class="shadow" width="500px">
</center>

Note that because the indexer consumes a lot of memory, Cloudron might decide to not run it, if the mail server has
not been allocated enough memory. The status of the indexer is seen in the Email view:

<center>
<img src="/img/email-fts-status.png" class="shadow" width="500px">
</center>

Emails are automatically indexed as they come in and no manual intervention is required. If you enable indexing on a
server with existing mails, the first search triggers indexing of that mailbox. If that mailbox has a lot of mail,
then the first search can take a long time - maybe even up to 10 mins for 1GB of mail, it all depends on how fast your
server is.

Both, [Roundcube](/apps/roundcube/#search) and [SOGo](/apps/sogo/#search) can take advantage of indexed search.

!!! note "High Resource use"
    Solr (the indexer) consumes a lot of memory and disk space. To use it, you must allocate atleast 3GB for the mail service.

## Recursive Search

Recursive Search in IMAP is implemented using `MULTISEARCH` command. However, support for this is non-existent in mail clients.

Roundcube, Thunderbird and SOGo implement recursive search natively.

For other clients like SnappyMail, there is a Virtual `All Mail` folder. The `All Mail` folder contains all the emails across all folders in a single virtual folder. By providing a single folder, SnappyMail can search through all emails easily.

The `All Mail` folder can be enabled in the Email Settings:

<center>
<img src="/img/email-virtual-all-mail-setting.png" class="shadow" width="500px">
</center>

See this [blog post](https://blog.cloudron.io/email-search-in-mail-clients/) for more information.

## Mailbox Sharing

Mailbox Sharing feature allows users to share their mailboxes with each other using IMAP ACLs.

Mailbox Sharing can be enabled in the `Email` view:

<center>
<img src="/img/email-mailbox-sharing.png" class="shadow" width="500px">
</center>

Once enabled, users can share their mailboxes using webmail like SOGo and Roundcube.

For example, in Roundcube, a user can share a folder of their mail account with another user like below:

<center>
<img src="/img/email-sharing.png" class="shadow" width="500px">
</center>

The other user can see the shared folder in their account:

<center>
<img src="/img/email-shared.png" class="shadow" width="500px">
</center>

!!! note "Manual migration"
    Mailbox sharing does not work out of the box for Cloudrons that were installed before 6.0.
    Follow [this guide](/guides/mailbox-sharing) to migrate.

## Alternate MX

A failover/alternate/backup MX can be setup for a domain on Cloudron. To set this up, do the following:

* Enable incoming mail for the Cloudron domain

* Cloudron has a anti-spoof feature to ensure that only it can generate emails for the incoming domains. This
feature will prevent the external MX from forwarding emails to it. However, Cloudron skips this spoof check
for servers listed in the domain's SPF record. So, white list the MX's IP address block in the domain's SPF record.
Note that it is necessary to specifically whitelist the server(s). Just a permissive SPF with `~all` and `?all` is not
enough.

## Archived Emails

When deleteing a mailbox, you can choose whether to delete the content (emails) inside the mailbox.
If you choose to keep the emails, then you can find the archived emails at `/home/yellowtent/boxdata/mail/vmail/`.

## Autodiscover

Auto discover is a feature where an email client is able to automatically detect the SMTP and IMAP configuration
from the email address. Setting up auto discover is very client specific.

### Autoconfig.xml

The [autoconfig.xml config format](https://wiki.mozilla.org/Thunderbird:Autoconfiguration:ConfigFileFormat) is supported by
many email clients like Thunderbird, K-9, KMail, Evolution, Kontact & others. Cloudron automatically serves up the autoconfig.xml
for all configured mail domains. Note that this feature requires an app to be installed on each of the email bare domains
i.e `domain1.com`, `domain2.com`, `domain3.com` and so on. These bare domains can also be just [redirects](/apps/#redirections)
to an existing app. If the bare domain is hosted outside Cloudron, see this [forum post](https://forum.cloudron.io/topic/4742/autoconfig-for-mail-when-website-not-hosted-on-cloudron)
for instructions.

!!! note "Requires app on email domain"
    Cloudron can serve `autoconfig.xml` only if there is an app installed on the email domain address. For example,
    if the email address `girish@cloudron.example` , you must have an app (or redirect/alias) at `cloudron.example`
    and `https://cloudron.example` should work.

To test if the file is being served up, run the following command and validate the output against the [spec](https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Autoconfiguration):

```
    curl https://cloudron.example/.well-known/autoconfig/mail/config-v1.1.xml
```

### Autodiscover.xml

The [autodiscover.xml config format](https://learn.microsoft.com/en-us/previous-versions/office/developer/exchange-server-interoperability-guidance/hh352638(v=exchg.140)) is a process by which a Microsoft Exchange Server client can determine the URL of a particular Microsoft Exchange ActiveSync endpoint. Cloudron supports ActiveSync via [SOGo](/apps/sogo). Note that only Outlook is known to implement this protocol.

If your email address is `girish@cloudron.example`, you must configure the app hosted at `cloudron.example`, to serve up a file named `autodiscover/autodiscover.xml` i.e `https://cloudron.example/autodiscover/autodiscover.xml` must have the file below:

```
<Autodiscover>
  <Response>
    <Culture>en:us</Culture>
    <Action>
      <Settings>
        <Server>
          <Type>MobileSync</Type>
          <Url>https://sogo.cloudron.example</Url>
          <Name>https://sogo.cloudron.example</Name>
        </Server>
      </Settings>
    </Action>
  </Response>
</Autodiscover>
```

### Outlook

!!! warning "Doesn't work"
    This method does not seem to work with Microsoft Outlook 2016, Microsoft Outlook 2019 or Microsoft Outlook for Office 365.
    Your mileage may vary.

Outlook performs an HTTP POST request to the following in order:

* `https://example.com/autodiscover/autodiscover.xml`
* `https://autodiscover.example.com/autodiscover/autodiscover.xml`
* DNS SRV lookup of `_autodiscover._tcp.example.com` and use the response to look up `https://srvresponse/autodiscover/autodiscover.xml.
  The SRV record is of the format `0 0 443 ssl.mailprovider.com`.

Because, it is a POST request, you cannot use some static hosting setup. For example, you can use a PHP file like below (there are also
more [sophisticated](https://github.com/gronke/email-autodiscover) attempts):

```
<?php
$raw = file_get_contents('php://input');
$matches = array();
preg_match('/<EMailAddress>(.*)<\/EMailAddress>/', $raw, $matches);
header('Content-Type: application/xml');
?>
<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">
  <Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">
    <User>
      <DisplayName>Cloudron</DisplayName>
      <EMailAddress><?php echo $matches[1]; ?></EMailAddress>
    </User>
    <Account>
      <AccountType>email</AccountType>
      <Action>settings</Action>
      <Protocol>
        <Type>IMAP</Type>
        <Server>my.example.com</Server>
        <Port>993</Port>
        <DomainRequired>off</DomainRequired>
        <SPA>off</SPA>
        <SSL>on</SSL>
        <AuthRequired>on</AuthRequired>
        <LoginName><?php echo $matches[1]; ?></LoginName>
      </Protocol>
      <Protocol>
        <Type>SMTP</Type>
        <Server>my.example.com</Server>
        <Port>587</Port>
        <DomainRequired>off</DomainRequired>
        <SPA>off</SPA>
        <Encryption>TLS</Encryption>
        <AuthRequired>on</AuthRequired>
        <LoginName><?php echo $matches[1]; ?></LoginName>
      </Protocol>
    </Account>
  </Response>
</Autodiscover>
```

## SRS

[Sender Rewriting Scheme](https://en.wikipedia.org/wiki/Sender_Rewriting_Scheme) is a scheme for
rewriting the envelope sender address of an email message. This method was devised to forward email
without breaking SPF.

Cloudron mail server implements SRS for forwarded emails. Cloudron's SRS implementation is in-line
with email providers like [Office 365](https://docs.microsoft.com/en-us/office365/troubleshoot/antispam/sender-rewriting-scheme)
and [namecheap](https://www.namecheap.com/support/knowledgebase/article.aspx/10204/2214/email-forwarding-why-its-important-to-have-srs-configured/).

## TLS version

There exist email servers in the wild that use old and obsolete TLS protocols like SSLv3, TLSv1 and TLSv1.1.
By default, all these protocols are disabled on the mail server since they are insecure. You can double check
if the mail server is using these old protocols using `nmap --script ssl-enum-ciphers -p 25 <mailserverip>`.

If you want to enable support for these insecure protocols, you can do the following:

* `docker exec -ti mail /bin/bash`
* Edit `/run/haraka/config/tls.ini` and the line `minVersion=TLSv1`. Add this line to the beginning of the file.
* `supervisorctl restart haraka`

Note that the setting is not persistent across mail container and server restarts. So, you have to add this line
by hand, if those events happen.

## Server status

The status of the DNS and Email delivery is available in the `Status` tab of the mail domain (`Email` -> `Select Domain` -> `Status`).

<center>
<img src="/img/email-checks.png" class="shadow" width="500px">
</center>

It's a good idea to check your spam score periodically at [mail-tester.com](https://www.mail-tester.com/).
This gives a view of how rest of the world views your email server.

### DNS Records

#### SPF

SPF records specify which servers are allowed to send emails on behalf of a domain name.
When you setup Cloudron, it will create a SPF record and set itself as the sender. If a SPF record
already exists, it will add itself to the existing record.

If you use a [relay](#relay-outbound-mails), you must modify the SPF record to allow the relay
to send emails on behalf of the domain. Please refer to your relay provider's documentation on
how to setup SPF.

The [SPF project](http://www.open-spf.org/) has detailed information on the syntax of this record.

!!! note "SPF Record Type"
    Some DNS providers list a DNS record type of `SPF`. This DNS record is [obsolete](https://en.wikipedia.org/wiki/List_of_DNS_record_types#Obsolete_record_types). Use a TXT record instead.

#### DKIM

DKIM records specify a public key that can be used to authenticate mails from a domain. The rough idea is
to generate a public/private key and use the private key to sign all outbound mails. The public key is listed
in the DNS and can be used to verify the email.

Cloudron automatically generates a DKIM key pair for each domain and sets up the DNS with the selector `cloudron-<uniqueid>`.
The unique id suffix is necessary for the domain to be used across multiple Cloudrons.

#### DMARC

DMARC is a protocol that uses SPF and DKIM to detect email spoofing. For DMARC validation to succeed, along
with SPF or DKIM check, DMARC alignment needs to succeed as well.

By default, Cloudron sets up DMARC records to reject all mails that fail SPF/DKIM validation. This way the
Cloudron administrator can feel fairly safe that nobody else is sending mails with their domain.

```
$ host -t TXT _dmarc.girish.in
_dmarc.girish.in descriptive text "v=DMARC1; p=reject; pct=100"
```

When using a [relay](#relay-outbound-mails), please check your service provider documentation to see how to make
sure DMARC alignment can succeed.

#### MX record

The MX record is required to receive mail. If you unable to receive mail despite this being set,
check if the [outbound port 25](#outbound-ports) is open.

#### DANE

[DANE](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities) provides a mechanism to authenticate
TLS certificates without a certificate authority (CA).

DNSSEC is a prerequisite for DANE.

DANE record has to be set up manually. Use [this site](https://www.huque.com/bin/gen_tlsa) or the `tlsa` tool to
generate records. On Cloudron, the private key for a domain does not change. For this reason, it is safe to choose
`SPKI: Use subject public key` as the selector and `DANE-EE: Domain Issued Certificate` for usage.

```
$ tlsa --usage 3 --selector 1 --mtype 1 --port 25 my.cloudron.example
Got a certificate for 185.232.70.47 with Subject: /CN=*.cloudron.example
_25._tcp.my.cloudron.example. IN TLSA 3 1 1 c93c8d09af08595d90c2b59319277eb12eaeb2f7f0fada65a5f23a5b18746110
Got a certificate for 2a03:4000:4e:9d:b4a5:5aff:fec0:3347 with Subject: /CN=*.cloudron.example
_25._tcp.my.cloudron.example. IN TLSA 3 1 1 c93c8d09af08595d90c2b59319277eb12eaeb2f7f0fada65a5f23a5b18746110
```

#### PTR record

PTR records or rDNS or reverse DNS are DNS entries that can be used to resolve an IP address to a
fully-qualified domain name. For example, the PTR record of the IP 1.2.3.4 can be looked up as
`host -t PTR 4.3.2.1.in-addr.arpa`.

In the context of email, the PTR record must match the [server location](#server-location). A PTR
record is required only when sending email directly from the server. If you are using a
[mail relay](#relay-outbound-mails), you do not need to set the PTR record.

**The PTR record is set by your VPS provider and not by your DNS provider.**. For example,
if your server was created in Digital Ocean, you must go to Digital Ocean to set the PTR
record.

We have collected a few links to help you set the PTR record for different VPS:

* **AWS EC2 & Lightsail** - Fill the [PTR request form](https://aws-portal.amazon.com/gp/aws/html-forms-controller/contactus/ec2-email-limit-rdns-request).

* **Digital Ocean** - Digital Ocean sets up a PTR record based on the droplet's name. So, simply rename
your droplet to `my.<domain>`.

* **Hetzner** - Follow this [guide](https://docs.hetzner.com/cloud/servers/cloud-server-rdns/).

* **Linode** - Follow this [guide](https://www.linode.com/docs/guides/configure-your-linode-for-reverse-dns/).

* **Netcup** -  You can enter a reverse lookup in the customer area CCP for your vServer - [wiki doc](https://www.netcup-wiki.de/wiki/Produkte_CCP#rDNS)

* **Scaleway** - You can also set a PTR record on the interface in their control panel.

* **Time4VPS** - Follow this [guide](https://www.time4vps.com/knowledgebase/change-hostname-reverse-dns-rdns-ptr/).

* **UpCloud** - The PTR can be set in the *Network* section of the VPS instance configuration.

* **Vultr** - The PTR record can be set for each public IP in the Vultr server settings and is called `Reverse DNS`. It takes about 24h to propagate, so make sure to do this well in advance of enabling email.

* **Home servers** - The PTR record has to be set by your ISP. Customers on Business plans (like Comcast Business) can send a support request to their support to have the PTR set.

Once setup, you can verify the PTR record [here](https://mxtoolbox.com/ReverseLookup.aspx).

### SMTP Status

#### Outbound SMTP

To send email, outbound port 25 needs to be unblocked. Most VPS providers block outbound port 25
as a spam control measure.

For some providers like Digital Ocean, [Vultr](https://www.vultr.com/resources/faq/#outboundsmtp) and Linode,
you can contact their support to get port 25 unblocked. Providers like EC2 and Lightsail allow oubound
port but with a rate limit. For other providers like Google Cloud and home servers, you are left with
no choice but to set up a [relay](#relay-outbound-mails).

#### Blacklists

The server's IP plays a big role in how emails get handled. Cloudron automatically checks
the following services to see if your IP is blacklisted. If it is, please follow the links
below and contact the services below to get your IP removed (usually this involves filling up
some form).

* [Abuse.ch](http://abuse.ch/)
* [Barracuda](http://www.barracudacentral.org/rbl/removal-request)
* [Composite Blocking List](http://www.abuseat.org)
* [Multi SURBL](http://www.surbl.org)
* [Passive Spam Block List](https://psbl.org)
* [Sorbs Aggregate Zone](http://dnsbl.sorbs.net/)
* [Sorbs spam.dnsbl Zone](http://sorbs.net)
* [SpamCop](http://spamcop.net)
* [SpamHaus Zen](http://spamhaus.org)
* [The Unsubscribe Blacklist(UBL)](http://www.lashback.com/blacklist/)
* [UCEPROTECT Network](http://www.uceprotect.net/en)

You can also check [valli.org](http://multirbl.valli.org/) for an exhaustive IP blacklist check.

## Security

*   Cloudron checks against the [Zen Spamhaus DNSBL](https://www.spamhaus.org/zen/) before accepting mail.
*   Email can only be accessed with IMAP over TLS (IMAPS).
*   Email can only be relayed (including same-domain emails) by authenticated users using SMTP/STARTTLS.
*   Cloudron ensures that `MAIL FROM` is the same as the authenticated user. Users cannot spoof each other.
*   All outbound mails from Cloudron are `DKIM` signed.
*   Cloudron automatically sets up SPF, DMARC policies in the DNS for best email delivery.
*   All incoming mail is scanned via `Spamassasin`.

