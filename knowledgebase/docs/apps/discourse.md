# <img src="/img/discourse-logo.png" width="25px"> Discourse App

## About

Discourse is a platform for community discussion. Free, open, simple. 

* Questions? Ask in the [Cloudron Forum - Discourse](https://forum.cloudron.io/category/33/discourse)
* [Discourse Website](https://www.discourse.org/)
* [Discourse forum](https://meta.discourse.org/)

## Installing plugins

To install a plugin, open a [Web terminal](/apps#web-terminal) and run the following commands:

```
cd /app/code/plugins
git clone <plugin-repo>
chown -R cloudron:cloudron <plugin-dir>
cd /app/code
gosu cloudron:cloudron bundle exec rake plugin:install_all_gems
gosu cloudron:cloudron bundle exec rake db:migrate
gosu cloudron:cloudron bundle exec rake assets:precompile
```

Restart the application to double check everything works.

If the plugin modifies the posts in some way, you might want to [rebake posts](#rebaking-posts).

!!! warning "Install with care"
    Use caution when installing plugins since they might break your installation. The list of
    official plugins can be found [here](https://github.com/discourse/discourse/blob/main/lib/plugin/metadata.rb).

## Changing root account email

The email of the root account is `root@cloudron.local`. Discourse sends an activation email to the old
email address to switch email. Since we don't have access to the default email account, we have to use
the rails console to switch the email.

Open  a [Web terminal](/apps#web-terminal) and run the following commands:

```
# cd /app/code
# gosu cloudron:cloudron bundle exec script/rails console
irb(main):001:0> u = User.find_by_username("root")
irb(main):002:0> u.email = "YOUR_NEW_EMAIL_ADDRESS"
irb(main):004:0> u.save!
```

## Changing domain

When changing the domain of an existing discourse installation, Cloudron automatically
rebuilds the assets. However, the posts in the forum are not re-written. To rebake
the posts, open a [Web terminal](/apps#web-terminal) and run the following
command:

```
# cd /app/code
# gosu cloudron:cloudron bundle exec ruby script/discourse remap old.domain.com new.domain.com
```

## Rebaking posts

To rebuild all posts (for example, to apply formatting provided by a newly installed plugin to
old posts), open a [Web terminal](/apps#web-terminal) and run the following
command:

```
# cd /app/code
# gosu cloudron:cloudron bundle exec rake posts:rebake
```

## Incoming email setup

* Create a mailbox to send and receive emails in Discourse
* Set both the `Mail FROM Address` and `Incoming mail` to the same mailbox.
    <center>
    <img src="/img/discourse-mail-setting.png" class="shadow" width="500px">
    </center>

!!! warning "Use same mailbox"
    Discourse does not support having different mail from and incoming mail address

* To enable creation of new topics via email, go to the settings page of a category and set a custom incoming email address.
    <center>
    <img src="/img/discourse-category-email-setup.png" class="shadow" width="500px">
    </center>
