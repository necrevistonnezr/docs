# <img src="/img/privatebin-logo.png" width="25px"> PrivateBin App

## About

PrivateBin is a minimalist, open source online pastebin where the server has zero knowledge of pasted data. Data is encrypted/decrypted in the browser using 256 bits AES. 

* Questions? Ask in the [Cloudron Forum - PrivateBin](https://forum.cloudron.io/category/54/privatebin)
* [PrivateBin Website](https://privatebin.info/)
* [PrivateBin issue tracker](https://github.com/PrivateBin/PrivateBin/issues)

## Customizations
 
Various PrivateBin settings can be configured by editing `/app/data/conf.php` using
the [File manager](/apps#file-manager).

## Custom template

You can set a [custom template](https://github.com/PrivateBin/PrivateBin/wiki/Templates)
as follows:

* Create the template in `/app/data/custom_template/custom.php`. The name `custom.php` is
  hardcoded in the package.

* Change the template name in `/app/data/conf/conf.php` to be `custom`.

* You can save additional js/css/img in `/app/data/custom_template/` and access them from the
  php script as `js/custom/..`, `css/custom/...`, `img/custom/...`.

