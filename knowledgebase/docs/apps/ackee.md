# <img src="/img/ackee-logo.png" width="25px"> Ackee App

## About

Ackee is a Self-hosted, Node.js based analytics tool for those who care about privacy. 

* Questions? Ask in the [Cloudron Forum - Ackee](https://forum.cloudron.io/category/125/ackee)
* [Ackee Website](https://ackee.electerious.com/)
* [Ackee docs](https://docs.ackee.electerious.com/)
* [Ackee issue tracker](https://github.com/electerious/Ackee/issues)

## Admin Password

The admin password can be changed by editing `/app/data/env` using the [File manager](/apps/#file-manager). Be
sure to restart the app after making the change.

## Adding a domain

* First, add the domain to be tracked, inside Ackee's Settings page. For example, if the website `www.cloudron.space` is to be tracked, add it like so:

  <center>
  <img src="/img/ackee-add-domain.png" class="shadow" width="500px">
  </center>

* Next, you must configure Ackee with the correct [CORS configuration](https://docs.ackee.electerious.com/#/docs/CORS%20headers#platforms-as-a-service-configuration) for the domain you added. To do so, edit `/app/data/env` using the [File manager](/apps/#file-manager) and add the tracked website in `ACKEE_ALLOW_ORIGIN`.

```
ACKEE_ALLOW_ORIGIN="https://www.cloudron.space"
```

* Restart Ackee after making the above change.

* Embed Ackee's tracker.js script in the tracked website. Be sure to replace the ackee URL and the domain ID. You can get the snippet below by
  clicking on the domain entry in Ackee's setting view as well.

```
<script async src="https://ackee.cloudron.space/tracker.js" data-ackee-server="https://ackee.cloudron.space" data-ackee-domain-id="217118b9-1843-4462-82ba-2e0acd189b91"></script>
```

  <center>
  <img src="/img/ackee-embed-script.png" class="shadow" width="500px">
  </center>

## Data collection

By default, Ackee won't track personal information like device and browser info. To enable detailed tracking, pass the `data-ackee-opts` to the script tag:

```
<script async src="https://ackee.cloudron.space/tracker.js" data-ackee-server="https://ackee.cloudron.space" data-ackee-domain-id="217118b9-1843-4462-82ba-2e0acd189b91" data-ackee-opts='{ "ignoreLocalhost": true, "detailed": true }'></script>
```

You should now be able to see detailed stats:

  <center>
  <img src="/img/ackee-detailed-stats.png" class="shadow" width="500px">
  </center>

See Ackee's [docs](https://github.com/electerious/Ackee/blob/master/docs/Anonymization.md#personal-data) for more information.

