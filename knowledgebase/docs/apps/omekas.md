# <img src="/img/omekas-logo.png" width="25px"> OmekaS App

## About

Omeka provides open-source web publishing platforms for sharing digital collections and creating media-rich online exhibits.

* Questions? Ask in the [Cloudron Forum - OmekaS](https://forum.cloudron.io/category/130/omekas)
* [OmekaS Website](https://omeka.org/s/)
* [OmekaS issue tracker](https://github.com/omeka/omeka-s/issues)

## User Management

Cloudron user-management is supported by the pre-installed Ldap module where user email and name attributes settings with `mail` and `username`. The default user role is "researcher", the lower role. Please note that users who connect this way must login with their username and not their email address.

## Modules

Themes and modules can be installed via the [Web terminal](/apps/#web-termina) or via the file manager UI. Take care to verify compatibility withe Omeka S version and preinstalled components with Cloudron Omeka S app.

## CORS

```
<IfModule mod_headers.c>
    <FilesMatch "\.json$">
        Header add Access-Control-Allow-Origin "*"
        Header add Access-Control-Allow-Headers "origin, x-requested-with, content-type"
        Header add Access-Control-Allow-Methods "GET, POST, OPTIONS"
    </FilesMatch>
</IfModule>
```

