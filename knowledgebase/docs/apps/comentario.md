# <img src="/img/comentario-logo.png" width="25px"> Comentario App

## About

Comentario is an open-source web comment engine, which adds discussion functionality to plain, boring web pages.

* Questions? Ask in the [Cloudron Forum - Comentario](https://forum.cloudron.io/category/190/comentario)
* [Comentario Website](https://comentario.app/en/)
* [Comentario Docs](https://docs.comentario.app/en/getting-started/)
* [Comentario Source](https://gitlab.com/comentario/comentario)

## Identity Providers

Comentario supports login for users and commenters via many identity providers like Facebook, Google, GitHub. These can be configured
by editing `/app/data/secrets.yaml` using the [File Manager](/apps/#file-manager).

See [upstream docs](https://docs.comentario.app/en/configuration/backend/secrets/) for available providers and how to configure them.

Be sure to restart the app after making any changes.

## Custom Config

Custom configuration can be added by editing `/app/data/env.sh` using the [File Manager](/apps/#file-manager).
See [upstream docs](https://docs.comentario.app/en/configuration/backend/static/) for available options.

Be sure to restart the app after making any changes.


