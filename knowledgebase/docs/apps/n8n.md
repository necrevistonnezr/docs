# <img src="/img/n8n-logo.png" width="25px"> n8n App

## About

n8n Free and open fair-code licensed node based Workflow Automation Tool.

* Questions? Ask in the [Cloudron Forum - n8n](https://forum.cloudron.io/category/129/n8n)
* [n8n Website](https://n8n.io)
* [n8n forum](https://community.n8n.io)
* [n8n issue tracker](https://github.com/n8n-io/n8n/issues)

## Custom env

Custom environment variables can be set in `/app/data/env.sh` using the [File manager](/apps/#file-manager).
Be sure to restart the app after making changes.

## Timezone

To set the timezone, set the `GENERIC_TIMEZONE` [environment variable](https://docs.n8n.io/getting-started/installation/docker-quickstart.html#setting-timezone) in `/app/data/env.sh`. Be sure to restart the app after setting the timezone.

## Built-in node modules

n8n allows using built-in node modules. To use an built-in node module, edit `/app/data/env.sh`, add a line like below and restart the app:

```
export NODE_FUNCTION_ALLOW_BUILTIN=crypto
```

See [upstream docs](https://docs.n8n.io/getting-started/installation/advanced/configuration.html#use-built-in-and-external-modules-in-function-nodes) for more information.

## Custom node modules

To install custom node modules, edit `/app/data/env.sh` using the [File manager](/apps/#file-manager):

```
# note: this is a space separated list
export EXTRA_NODE_MODULES="handlebars@4.7.7 jsonata@2.0.2 marked@4.3.0"
```

The modules have to be whitelisted for use:

```
# note: this is a comma separated list
export NODE_FUNCTION_ALLOW_EXTERNAL=handlebars,jsonata,marked
```

Restart the app and use the module in Function nodes. Restarting the app will install the modules specified in `EXTRA_NODE_MODULES` automatically.
See [upstream docs](https://docs.n8n.io/getting-started/installation/advanced/configuration.html#use-built-in-and-external-modules-in-function-nodes) for more information.

<center>
<img src="/img/n8n-custom-node-module.png" class="shadow" width="500px">
</center>

## CLI

To use the n8n [CLI](https://docs.n8n.io/reference/cli-commands/), open a [Web terminal](/apps/#web-terminal):

```
# gosu cloudron /app/code/node_modules/.bin/n8n export:workflow --backup --output=/tmp/n8n/
```

