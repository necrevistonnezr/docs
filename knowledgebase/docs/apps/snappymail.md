# <img src="/img/snappymail-logo.png" width="25px"> SnappyMail App

## About

SnappyMail is a simple, modern & fast web-based email client.

* Questions? Ask in the [Cloudron Forum - SnappyMail](https://forum.cloudron.io/category/138/snappymail)
* [SnappyMail Website](https://snappymail.eu/)
* [SnappyMail issue tracker](https://github.com/the-djmaze/snappymail/issues)

## Default Setup

SnappyMail is pre-configured for use with Cloudron Email. The app automatically
generates domain configuration for all the apps that have email enabled at
_installation_ time. If you enable or disable email on one or more domains,
simply reconfigure the app and it will re-generate the necessary configuration.

## 2FA

SnappyMail has 2FA support built-in, it requires activating the "Two Factor Authentication" plugin as admin.

Activate the admin panel as [explained below](#Admin-panel), log in as admin, activate the plugin. Afterwards, users can activate and configure 2FA under Settings.

## Plugins

Snappymail has several plugins that can be activated in the [admin panel](#Admin-panel).

Rule of thumb (as for all software with plugins): Activate as little as possible for maximum stability.

## Multi-domain Setup

There are two ways to use SnappyMail when using Cloudron Email with multiple
domains.

* Users can login with their email and password to access their mailbox. If the
  Cloudron has two domains, `example1.com` and `example2.com`, the user can login
  using `user@example1.com` and `user@example2.com`. Aliases can be added as identities
  under SnappyMail settings.

* Users can login using one of the email domains and add the other domains
  using the `Add Account` dialog. For example, user can login as `user@example1.com`
  and add `user@example2.com` in the `Add Account` dialog.

    !!! note "Multiple accounts"
        SnappyMail tracks accounts based on the login email. This means
        that in the example above, if the user logs in `user@example2.com`, the
        `user@example1.com` id will not show up.

## External domains

To add one or more external domains, add them in SnappyMail's [admin panel](#admin-panel).

## Filters

SnappyMail has simple filters built-in (see Settings - Filters). 

More complex filters are possible but require knowledge of the Sieve language to as explained e.g. in these tutorials:

* [https://p5r.uk/blog/2011/sieve-tutorial.html](https://p5r.uk/blog/2011/sieve-tutorial.html)
* [https://docs.gandi.net/en/gandimail/sieve/sieve_tutorial.html](https://docs.gandi.net/en/gandimail/sieve/sieve_tutorial.html)
* [https://www.fastmail.help/hc/en-us/articles/360060591373-How-to-use-Sieve](https://www.fastmail.help/hc/en-us/articles/360060591373-How-to-use-Sieve)

### Vacation Email

An out of office / vacation mail message can be setup using Sieve filters.

```
require ["vacation"];
if true
{
	vacation :subject "Out of office" "Off to the alps!";
}
```

!!! note "At most once a day"
    Vacation messages are sent at most once a day to the same sender. This setting cannot
    be changed.

### Forwarding all emails

To forward all emails to an external mail, setup a Sieve filter as follows:

```
require ["copy","fileinto","vacation"];

if true
{
	redirect :copy "test@cloudron.io";
}
```

## Admin panel

The admin panel is located at `/?admin` and is disabled by default.

To enable it:

* Open a [File manager](/apps#file-manager) and edit the file `/app/data/_data_/_default_/configs/application.ini`.
Set the value of `allow_admin_panel` to `On`.

* Visit `https://snappymail.domain.com/?admin` on your browser. When you visit this URL, the admin password is generated
at `/app/data/_data_/_default_/admin_password.txt`.

* You can now login with the username `admin` and the password located at `/app/data/_data_/_default_/admin_password.txt`.

* The file `/app/data/_data_/_default_/admin_password.txt` may be deleted after you made a note of the password.

* Please change the password immediately.

!!! warning "Disable admin panel after use"
    We highly recommend disabling the admin panel after use.

## Attachment size

To adjust the attachment size, change the PHP configuration and the SnappyMail configuration.

* For PHP configuration, edit `/app/data/php.ini` using the [File manager](/apps/#file-manager) and add
```
[PHP]
upload_max_filesize = 40M
post_max_size = 40M
```

* For SnappyMail configuration, edit `/app/data/_data_/_default_/configs/application.ini` using the
[File Manager](/apps/#file-manager) and adjust `attachment_size_limit`.


Be sure to restart the app after making the above changes.

In addition to above, you also have to configure the mail server to allow [large email](/email/#max-mail-size).

