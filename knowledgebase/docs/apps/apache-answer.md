# <img src="/img/apache-answer-logo.png" width="25px"> Apache Answer App

## About

Apache Answer is a Q&A platform software for teams at any scales.

* Questions? Ask in the [Cloudron Forum - Apache Answer](https://forum.cloudron.io/category/188/apache-answer)
* [Apache Answer Website](https://answer.apache.org/)
* [Apache Issue Tracker](https://github.com/apache/incubator-answer/issues)


