# <img src="/img/woodpecker-logo.png" width="25px"> The Woodpecker App

## About

Woodpecker is a simple CI engine with great extensibility. It runs your pipelines inside Docker containers.

* Questions? Ask in the [Cloudron Forum - Woodpecker](https://forum.cloudron.io/category/67/the-lounge)
* [Woodpecker Website](https://woodpecker-ci.org)
* [Woodpecker docs](https://woodpecker-ci.org/docs/intro)
* [Woodpecker issue tracker](https://github.com/woodpecker-ci/woodpecker/issues)

## Authentication

Woodpecker authenticates against your existing version control system (Github/Gitea/Gogs/GitLab etc). See the [upstream docs](https://woodpecker-ci.org/docs/administration/vcs/overview) for the supported providers.

For example, to use Gitea for authentication, create an OAuth2 application inside Gitea and edit `/app/data/env.sh` like so:

```
export WOODPECKER_GITEA=true
export WOODPECKER_GITEA_URL=https://gitea.cloudron.example
export WOODPECKER_GITEA_CLIENT=1950a7f0-74e1-4003-8d51-6253eb673940
export WOODPECKER_GITEA_SECRET=gto_kgoizrbde5qv242q2waw7hocb5sa5snafqhwoeownflb4lml7dsa
```

Be sure to restart the app after making any changes.

!!! warning "Only one auth provider"
    Only one auth provider can be active at a time. For example, if you enabled Gitea, make sure all other values like `WOODPECKER_GITLAB` are removed or set to false.

## Agent

Agents are workers that execute the pipelines. Agents must be installed in a separate VM.

Agent binaries are available [here](https://github.com/woodpecker-ci/woodpecker/releases/). A shared secret token is required to run the agent. This token is automatically generated and located at `/app/data/env.sh`. Please use the [File manager](/apps/#file-manager) to make a note of this value and use it in the `WOODPECKER_AGENT_SECRET` variable below.

Documentation of agent variables can be found [here](https://woodpecker-ci.org/docs/administration/agent-config).

To run the agent using Docker:

```
docker run --name=woodpecker-agent --restart=always --detach \
    -e WOODPECKER_SERVER="woodpecker.cloudron.example:9000" \
    -e WOODPECKER_MAX_WORKFLOWS=4 \
    -e WOODPECKER_GRPC_SECURE=true \
    -e WOODPECKER_LOG_LEVEL=info \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -e WOODPECKER_BACKEND=docker \
    -e WOODPECKER_AGENT_SECRET="see /app/data/env.sh for the value" \
    woodpeckerci/woodpecker-agent:latest
```

You can view the agent logs using `docker logs -f woodpecker-agent`.

```
# docker logs -f woodpecker-agent
{"level":"debug","time":"2023-04-27T09:06:42Z","message":"loaded docker backend engine"}
{"level":"debug","time":"2023-04-27T09:06:42Z","message":"request next execution"}
```

!!! warning "Do not install agent on Cloudron VM"
    Do not install the agent on Cloudron server itself. This is dangerous because the agent has
    full access to docker and it can (accidentally) delete or corrupt your apps.

## Customization

To customize, edit `/app/data/env.sh` using the [File manager](/apps/#file-manager). Be sure to restart the app after making any changes.

See [upstream docs](https://woodpecker-ci.org/docs/administration/server-config) for all the server configuration options.

## Registration

By default, users from the configured [authentication provider](#authentication) can login to Woodpecker. If you want to prevent new users
from signing in, you can disable this by setting `export WOODPECKER_OPEN=false` in `/app/data/env.sh`.

