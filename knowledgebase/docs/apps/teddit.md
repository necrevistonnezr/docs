# <img src="/img/teddit-logo.png" width="25px"> Teddit App

## About

Teddit is a free and open source alternative Reddit front-end focused on privacy.

* Questions? Ask in the [Cloudron Forum - Teddit](https://forum.cloudron.io/category/169/teddit)
* [Teddit Website](https://teddit.net)
* [Teddit issue tracker](https://codeberg.org/teddit/teddit/issues)

## Customization

A variety of settings are customizable via [config.js](https://codeberg.org/teddit/teddit/src/branch/main/config.js.template).

To customize, edit `/app/data/config.js` using the [File manager](/apps/#file-manager) and restart the app.

