# <img src="/img/tandoor-logo.png" width="25px"> Tandoor App

## About

Manage your ever growing recipe collection online. Drop your collection of links and notes.

* Questions? Ask in the [Cloudron Forum - Mealie](https://forum.cloudron.io/category/163/tandoor)
* [Tandoor Website](https://tandoor.dev)
* [Tandoor issue tracker](https://github.com/TandoorRecipes/recipes/issues)

## Custom config

Custom configuration can be added in `/app/data/env` using the [File manager](/apps/#file-manager).
See [upstream config file](https://github.com/TandoorRecipes/recipes/blob/develop/.env.template) for
the available options.

Be sure to restart the app after making any changes.

