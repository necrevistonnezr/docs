# <img src="/img/grav-logo.png" width="25px"> Grav App

## About

Grav is a modern open source flat-file CMS.

* Questions? Ask in the [Cloudron Forum - Grav](https://forum.cloudron.io/category/72/grav-cms)
* [Grav Website](https://getgrav.org)
* [Grav forum](https://discourse.getgrav.org/)
* [Grav issue tracker](https://github.com/getgrav/grav/issues)

## Admin Plugin

This package pre-installs the [Admin plugin](https://github.com/getgrav/grav-plugin-admin).
This admin plugin for Grav is an HTML user interface that provides a convenient way to configure Grav
and easily create and modify pages.

!!! note "Do not uninstall admin plugin"
    While the Admin Plugin is totally optional in the upstream project, this package is designed to work
    with the Admin Plugin. It should not be uninstalled.

## CLI

[GPM](https://learn.getgrav.org/17/cli-console/grav-cli-gpm) and [Grav](https://learn.getgrav.org/17/cli-console/grav-cli)
commands can be executed by opening a [Web terminal](/apps#web-terminal):

```
# cd /app/code
# sudo -u www-data -- /app/code/bin/gpm install bootstrap4-open-matter
# sudo -u www-data -- /app/code/bin/grav
```

## Skeletons

[Grav Skeletons](https://learn.getgrav.org/17/advanced/grav-development#grav-skeletons) are completely packaged
sample sites. They include plugins, themes, pages in one bundle. They can be downloaded from [here](https://getgrav.org/downloads/skeletons).

Skeletons don't work well with this Cloudron package:

* Skeletons provide the core and plugin files and they are often out-of-date. On Cloudron, core files are read only for
  update and security reasons.
* Skeletons may or may not contain the admin plugin. This package is designed for use with the admin plugin.

For this reason, it's best to use Grav Skeletons with the [LAMP App](/apps/lamp).

* Install LAMP app
* Upload the Skeleton zip file and extract it to the `/app/data/public` folder using the [File manager](/apps/#file-manager):

<center>
<img src="/img/grav-skeleton-extract.png" class="shadow" width="500px">
</center>

* Change the ownership of the `/app/data/public` directory to `www-data`.

<center>
<img src="/img/grav-skeleton-chown.png" class="shadow" width="500px">
</center>

* Your skeleton should be live!

