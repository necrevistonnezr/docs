# <img src="/img/bookstack-logo.png" width="25px"> BookStack App

## About

BookStack is a simple, self-hosted, easy-to-use platform for organising and storing information.

* Questions? Ask in the [Cloudron Forum - BookStack](https://forum.cloudron.io/category/29/bookstack)
* [BookStack Website](https://www.bookstackapp.com)
* [BookStack discord](https://discord.com/invite/ztkBqR2)
* [BookStack issue tracker](https://github.com/BookStackApp/BookStack/issues)

## Admin

When using Cloudron user management, BookStack's built-in admin user is disabled.
See the [BookStack docs](https://www.bookstackapp.com/docs/admin/ldap-auth/) for
more information. In addition, the app is pre-setup to give admin status to all users.
You can change this by going to `Settings` -> `Registration` and adjusting the value of
`Default user role after registration`. This way, the first user to login will be an admin
and the roles of rest of the users can be managed inside BookStack.

## Customization

[BookStack env vars file](https://github.com/BookStackApp/BookStack/blob/development/.env.example.complete) has a list of
various customizable options including default book view, default light/dark mode, timezone etc.

To change a setting, use the [File Manager](/apps#file-manager) to edit custom configuration in `/app/data/env`.
Be sure to restart the app after making any changes.

## External registration

Bookstack does not allow external users to register when Cloudron user management (LDAP)
is enabled. If you require external registration, install Bookstack with Cloudron user
management disabled.

See the [Bookstack docs](https://www.bookstackapp.com/docs/admin/third-party-auth/) to
enable 3rd party auth like Google, GitHub, Twitter, Facebook & others.

## Themes

Custom theme can be placed in `/app/data/themes` . You can edit the files here using the [File manager](/apps/#file-manager).

