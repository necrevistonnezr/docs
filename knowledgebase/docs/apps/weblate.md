# <img src="/img/weblate-logo.png" width="25px"> Weblate App

## About

Weblate is a libre web-based translation tool with tight version control integration.

* Questions? Ask in the [Cloudron Forum - Weblate](https://forum.cloudron.io/category/106/weblate)
* [Weblate Website](https://weblate.org)
* [Weblate docs](https://docs.weblate.org/en/latest/)
* [Weblate issue tracker](https://github.com/WeblateOrg/weblate/issues)

## Custom Config

[Custom configuration](https://docs.weblate.org/en/latest/admin/config.html) can be set in `/app/data/custom_settings.py`
using the [File Manager](/apps/#file-manager).

!!! note "Python configuration file"
    Please be aware that the configuration file is in Python. Python is sensitive to whitespace and indentantion.
    Check your configuration file syntax if Weblate is not applying your changes.

Example:

```
REGISTRATION_OPEN = False
```

Be sure to restart the app after making any changes.

## Celery Worker Options

Weblate has a few [celery worker](https://docs.weblate.org/en/weblate-4.2.2/admin/install.html#celery) for background processing.
Depending on the allocated resources and available CPUs it may be required to adjust the worker options.
Those can be set by editing the file at `/app/data/celery.env`:

The default file contains one env variable per worker:
```
export CELERY_MAIN_OPTIONS=""
export CELERY_NOTIFY_OPTIONS=""
export CELERY_TRANSLATE_OPTIONS=""
export CELERY_BACKUP_OPTIONS=""
export CELERY_BEAT_OPTIONS=""
```

To set for example the concurrency for one worker, adjust the corresponding line like:
```
export CELERY_MAIN_OPTIONS="--concurrency 16"
```

Then restart the app. If the app does not start up again, take a look at the logs to see if an option may not be accepted for a worker.

