# <img src="/img/openvpn-logo.png" width="25px"> OpenVPN App

## About

OpenVPN provides flexible VPN solutions to secure your data communications, whether it's for
Internet privacy, remote access for employees, securing IoT, or for networking Cloud data centers

* Questions? Ask in the [Cloudron Forum - OpenVPN](https://forum.cloudron.io/category/20/openvpn)
* [OpenVPN Website](https://openvpn.org/)
* [Upstream OpenVPN forum](https://community.openvpn.net/openvpn)

## Desktop and Mobile Clients

The OpenVPN app has been tested with the following clients:

* NetworkManager on Ubuntu
* [Tunnelblick](https://www.tunnelblick.net/) on Mac OS X
* [OpenVPN for Android](https://play.google.com/store/apps/details?id=de.blinkt.openvpn)

## Configs

The OpenVPN config can be downloaded using the download button. There are a couple of formats:

* `.ovpn` - For linux and OpenVPN app on Android
* `.tblk` - For Tunnelblick

<center>
<img src="/img/openvpn-config.png" class="shadow" width="500px">
</center>

## How to connect on Ubuntu

Download the .ovpn embedded certs config file from the OpenVPN app. Then, in the Network settings in Ubuntu,
click on 'Add VPN' and then `Import from file...`.

<center>
<img src="/img/openvpn-ubuntu.png" class="shadow" width="500px">
</center>

## Admin Settings

The admin panel can be used to customize some of the popular settings like
the network address, DNS server and client-to-client connectivity.

To make a user an admin, edit the file `/app/data/config.ini` and add
the username to the `admins` key. Note that you have to restart the app
and re-login for the admin role to take effect. A `Settings` button will
appear in the navigation bar.

<center>
<img src="/img/openvpn-settings.png" class="shadow" width="500px">
</center>

## Built-in DNS Server

This app has a built-in Dnsmasq DNS server which is pushed to clients. This DNS server
allows resolution of connected clients using `devicename.username`.

You can configure this DNS server by editing `/app/data/dnsmasq.conf` using the [File manager](/apps#file-manager).
For example, to make Dnsmasq forward DNS requests to an internal DNS server, use the following:

```
server=internal-server-ip
```

See [Dnsmasq docs](http://thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html) for all the available options.

## Custom DNS Server

You can configure the VPN to use a custom DNS server - for example, an external AdGuard or a Pi-Hole installation.
Simply, put in the IP of the DNS server in the [Admin Settings](/#admin-settings). In the screenshow below,
`104.207.150.252` is the AdGuard DNS Server IP.

<center>
<img src="/img/openvpn-adguard.png" class="shadow" width="500px">
</center>

## Customizations

You can customize various settings by editing `/app/data/openvpn.conf` using
the  [File Manager](/apps#file-manager). Some popular options are
discussed below:

### Custom routes

By default, clients are configured to route all traffic via the VPN. If you disable this, you would
want to push custom routes for the network and hosts behind the VPN. For example, edit the file as below and
restart the app.

```
# push "redirect-gateway def1 bypass-dhcp"
push "route 178.128.183.220 255.255.255.255"
push "route 178.128.74.0 255.255.255.0"
```

### Concurrent connections

By default, every connection must have one unique cert. With `duplicate-cn`, one cert can be used by more than one connection.

Enable this if multiple clients with the same VPN config can concurrently connect:

```
duplicate-cn
```

## Privacy

The OpenVPN app provides a tunnel to channel all the traffic from your
devices via the Cloudron. Websites and services that you visit will
not see the IP address of your devices but they *will* see the IP
address and possibly the RDNS hostname of your Cloudron.

You can check what sort of information can be gathered from your
Cloudron's IP address using [ipleak.net](https://ipleak.net).

## Custom Client Configuration

Custom Client Configuration allows the OpenVPN admin to assign a specific IP address to a client or push specific options 
such as compression and DNS server to a client.

To add custom settings:

* Edit `/app/data/openvpn.conf` and add the following line:
```
client-config-dir /app/data/ccd
```

* Create the directory `/app/data/ccd`

* You can create custom client configs in this directory by creating files with the name `[username]_[devicename]`. You can also
  create a file named `DEFAULT` which will be used if no device specific file exists.

* For example, to assign a static IP to a client, you can add the line `ifconfig-push 10.8.0.50 10.8.0.51` (requires IP pair)

* Restart the app for changes to take effect.

## IPv6

### Transport

If you have enabled [IPv6 support](/networking/#ipv6) on Cloudron, Clients can connect (the "transport" protocol) to the OpenVPN server via IPv6. Whether the connection uses IPv4 or IPv6 is a client OS preference.

### Inside Tunnel

IPv6 is supported inside the tunnel (independent of the transport protocol). The server needs to have an IPv6 address assigned for IPv6 tunneling to work.

## Troubleshooting

If you are unable to connect to the OpenVPN server, make sure that your VPS firewall
allows the OpenVPN port (by default, this is 7494/TCP). For example, you might have
to add this incoming port as part of EC2 security group.

