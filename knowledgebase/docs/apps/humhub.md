# <img src="/img/humhub-logo.png" width="25px"> HumHub App

## About

HumHub is a free and open-source social network software written on top of the Yii PHP
framework that provides an easy to use toolkit for creating and launching your own social network.

* Questions? Ask in the [Cloudron Forum - HumHub](https://forum.cloudron.io/category/155/humhub)
* [HumHub Website](https://humhub.com)
* [HumHub issue tracker](https://github.com/humhub/humhub/issues)

## Themes

Themes can be installed from the HumHub marketplace. In such a case, it is installed into the
module directory.

The HumHub [theme structure](https://docs.humhub.org/docs/theme/structure) also supports installation
into the `themes` subdirectory. For this, use the [File manager](https://cloudron.io/apps/#file-manager)
to create a subdirectory in `/app/data/themes` and upload the theme.

