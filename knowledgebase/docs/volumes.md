# Volumes

## Overview

Volumes are local or remote file systems. They can be used as an app's main data storage or as a shared storage location between apps.

## Add

Volumes can be added in the `Volumes` view. Click the `Add Volume` button to add a volume and select the mount type.

<center>
<img src="/img/volumes-list.png" class="shadow" width="500px">
</center>


Once added, the volumes can either be used as an app's [data directory](/apps/#data-directory) or be [mounted](/apps/#mounts) into one or more apps.

!!! note "Data Directory Limitation"
    Only volumes with Mount Type `EXT4` and `NFS` can be used as the [data directory](/apps/#data-directory), as other Mount Types do not properly support file permissions.

<center>
<img src="/img/volumes-app-storage.png" class="shadow" width="500px">
</center>

## Mount Type

Cloudron supports a variety of mount types. When using a mount type other than `No-op`, Cloudron will setup systemd mount config files
to automatically mount on server start up. These mount points are created under `/mnt/volumes`.

!!! warning "Do not create fstab entry"
    When using the NFS/EXT4/CIFS/SSHFS/XFS providers, do not add an entry in `/etc/fstab` because Cloudron will already set up the mount via systemd. Use the `No-op` provider if you want to add an `/etc/fstab` entry.

### CIFS

The CIFS mount type is used to mount CIFS shares. Note that, unlike EXT4 and NFS mount types, CIFS does not have a concept of
[users and groups](https://www.systutorials.com/docs/linux/man/8-mount.cifs/#lbAK). This makes it unsuitable for use as an
app's data directory but will work fine for volumes and backups.

<center>
<img src="/img/volumes-cifs.png" class="shadow" width="500px">
</center>

!!! note "Hetzner Storage Box"
    When using Hetzner Storage Box, the Remote Directory is `/backup` for the main account. For sub accounts,
    the Remote Directory is `/subaccount`.

### EXT4

The EXT4 mount type is used to mount external hard disks or block storage. To add an external EXT4 disk, first make sure the
disk is formatted as EXT4 using `mkfs.ext4 /dev/<device>`. Then, run `blkid` or `lsblk` to get the UUID of the disk.

<center>
<img src="/img/volumes-ext4.png" class="shadow" width="500px">
</center>

### Filesystem

The Filesystem type is used for giving apps access to directories on the server. These are just directories on the local
filesystem and do not require any mounting configuration. We recommend giving this directory `chmod 777` permissions for
maximum compatibility across apps.

<center>
<img src="/img/volumes-filesystem.png" class="shadow" width="500px">
</center>

As a security measure, only host paths under `/mnt`, `/media`, `/srv` and `/opt` are allowed.

### Filesystem (mountpoint)

When using the `mountpoint` mount type, Cloudron will not configure the server to mount the mount point. You have to set up `/etc/fstab`
or systemd mount config files on your own. Use this if you want to set up an unsupported mount type or want to add specialized
mount flags.

<center>
<img src="/img/volumes-mountpoint.png" class="shadow" width="500px">
</center>

As a security measure, only mount points under `/mnt`, `/media`, `/srv` and `/opt` are allowed.

### NFS

The NFS mount type is used to mount NFSv4 shares. If you need help setting up an NFS server, see [this article](https://www.tecmint.com/install-nfs-server-on-ubuntu).
By default, NFS shares will change the root user to be owned by the `nobody` user. This is done for security purposes since it
prevents creating files with setuid bit set. You can add `no_root_squash` to the options in the NFS server's exports file to
circumvent this.

<center>
<img src="/img/volumes-nfs.png" class="shadow" width="500px">
</center>

!!! warning "Insecure traffic"
    Please note that NFS traffic is unencrypted and can be tampered. For this reason, you must use NFS mounts only on secure private networks.

### Noop

When using the `No-op` type, only paths under `/mnt`, `/media`, `/srv`, `/opt` can be added for security reasons.

### SSHFS

The SSHFS mount type is used to mount a file system over SSH (using the SFTP protocol).

Cloudron does not support setting SSHFS volume as an app's data directory but will work fine for volumes and backups.

<center>
<img src="/img/volumes-sshfs.png" class="shadow" width="500px">
</center>

!!! note "Hetzner Storage Box"
    When using Hetzner Storage Box, the Remote Directory is `/home` for the main account. You can also leave this field empty.
    We have found sub accounts to be quite unreliable with SSFS. We recommend using CIFS instead if you want to use subaccounts.

### XFS

The XFS mount type is used to mount external hard disks or block storage. To add an external XFS disk, first make sure the
disk is formatted as XFS using `mkfs.xfs /dev/<device>`. Then, run `blkid` or `lsblk` to get the UUID of the disk.

<center>
<img src="/img/volumes-xfs.png" class="shadow" width="500px">
</center>

## Remount

Volumes can be remounted using the `Remount Volume` button. This is useful in situations where a networked volume got disconnected.

<center>
<img src="/img/volumes-remount.png" class="shadow" width="500px">
</center>

## File manager

The File Manager can be used to access the volume's file system from the browser. Use the File Manager button to open the File Manager:

<center>
<img src="/img/volumes-filemanager.png" class="shadow" width="500px">
</center>

Clicking the icon will pop up a new window. Note that there are action like Rename, Delete, Change Ownership
in the context menu.

## Sharing

Sharing a volume across apps can be tricky because each app is packaged differently and the run-as user of each app varies.

The permissions and ACL of the mount directory have to be carefully set to make it work across apps. The general idea is to
make file access work across multiple app containers by using the `media` group. The `media` group is currently hardcoded in the app containers to have the users www-data (uid 33) and cloudron (uid 1000). Most of the apps use one of these two users.

Do the following to prepare the volume for sharing:

* Identify the mount directory. For non-filesystem volumes, the mount directory is under `/mnt/volumes/<volume-id>`. For file system volumes, this is the host path. Set the `MOUNT_DIR` below accordingly.

* Run the following commands:

```
root@my:/# export MOUNT_DIR=/path/to/the/mount/directory
root@my:/# chmod 777 $MOUNT_DIR
root@my:/# chgrp media $MOUNT_DIR
root@my:/# chmod g+s $MOUNT_DIR
root@my:/# setfacl -d -m g::rwx $MOUNT_DIR
root@my:/# setfacl -d -m o::rx $MOUNT_DIR
```

The idea of the above commands is that newly created files are owned by the `media` group automatically. To double check that the changes were correctly applied:

```
root@my:/# touch $MOUNT_DIR/test-file
root@my:/# ls -l $MOUNT_DIR/test-file
-rw-rw-r-- 1 root media 0 Nov  3 22:41 /../test-file
```

As seen above, a newly created file has the correct permissions for access by `media` group automatically.

