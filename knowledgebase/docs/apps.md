# Apps

## Installation

Three types of app icons can be added in the dashboard:

* [App Store Apps](#appstore)
* [App Links](#app-links)
* [App Proxy](#app-proxy)

Icons can be added from the `Appstore` view:

<center>
<img src="../img/apps-add-app.png" class="shadow" width="600px">
</center>

### App Store

Apps can be installed from the `App Store` menu item. Clicking on an app will display
information about the app.

<center>
<img src="/img/apps-appstore-info.png" class="shadow" width="600px">
</center>

<br/>

Clicking the `Install` button will show an install dialog:

<center>
<img src="/img/apps-appstore-install.png" class="shadow" width="600px">
</center>

<br/>

### App Links

An App Link is a shortcut to an external web site. Clicking on a App Link simply opens up the website in a new tab
on the user's browser.

<center>
<img src="../img/apps-app-link.png" class="shadow" width="600px">
</center>

### App Proxy

App Proxy is a service that lets one publish a public HTTPS URL endpoint for a non-Cloudron hosted application.
When a user visits the public endpoint, App Proxy proxies requests to the hosted application.

When using HTTP proxying, you must ensure that the network between Cloudron and the internal application is secure.

Benefits of using the App Proxy include DNS management, Certificate management, Configurable domain aliases and redirections,
setting a custom CSP, setting custom `robots.txt` and up/down notifications.

You can also specify which users & groups can see the proxy app icon using the `Dashboard Visibility` setting.

<center>
<img src="../img/apps-proxy-app.png" class="shadow" width="600px">
</center>

The Upstream URI can have one for the following formats:

* `http://ip:port` or `http://[ipv6]:port`
* `https://ip:port` or `https://ipv6]:port`
* `http://domain:port`
* `https://domain:port`

When using `https`, certificates are not verified.

!!! warning "Updates & Backups"
    As the app is hosted externally, managing app updates and backups of proxied apps are outside the scope of Cloudron.

!!! warning "Cannot mirror public sites"
    App Proxy has not been designed to mirror or proxy other people's public sites or domains. The App Proxy
    sets the `Host` header to the App Proxy's location when proxying and not the upstream/target domain. You will most
    likely see a 502 error if you try to mirror public sites.

!!! note "Protect the upstream"
    It's a good idea to have a firewall rule in the upstream server to only accept requests from Cloudron server IP.

## Configuration

Clicking on the gear button will open the app's configure view.

<center>
<img src="/img/apps-configure-button.png" class="shadow" width="250px">
</center>

## Location

### Primary Domain

The `Location` field, also known as Primary Domain, is the subdomain into which the app will be installed. Use the drop down
selector on the right to choose the domain into which the app will by installed. If the subdomain field is empty,
the app will be installed in the bare/naked domain.

Cloudron packages are "relocatable" by design. Changing the location field in the `Location` section of the app's
configure UI will move the app to another domain or subdomain:

<center>
<img src="/img/apps-location.png" class="shadow" width="600px">
</center>

!!! note "Location field can be multi-level"
    The `Location` field can be any level deep. For example, you can specify location as
    `blog.dev` to make the app available at `blog.dev.smartserver.space`.

!!! note "No data loss"
    Moving an app to a new location is a non-destructive action. Existing app data will
    be migrated to the new domain.

### Secondary Domains

Some apps require more than one domain. For example, minio uses two separate domains - one for the UI and one it's API.
Other examples include Loomio (websockets domain), CryptPad (sandbox domain) and Traccar (OsmAnd protocol).

Secondary domains can be specified at installation time. Like the Primary domain, they can be changed later in the `Location` section:

<center>
<img src="/img/apps-secondary-domain.png" class="shadow" width="600px">
</center>

### Aliases

Some apps can be reached via more than one domain. For example, WordPress multi-site can serve up websites based on the domain name.
EspoCRM supports creating customers portals on custom domains.

Aliases can be setup from the `Location` section in the app's configure UI:

<center>
<img src="/img/apps-aliases.png" class="shadow" width="600px">
</center>

The alias feature is only enabled for select apps since it requires apps to support multiple domains.

### Redirections

Redirections forward one or more domains to the [primary domain](/#location) with a HTTP 301. They can be setup from the `Location` section in the app's configure UI:

<center>
<img src="/img/apps-redirect.png" class="shadow" width="600px">
</center>

In the above example, anyone visiting `chat2.cloudron.ml` or `chat3.smartserver.io` will be automatically
redirected to the main domain `chat.cloudron.ml` (with a HTTP 301).

The redirection feature preserves any URI components like subpaths in the original request.

!!! note "`www` redirection"
    In DNS, the domains `example.com` and `www.example.com` are independent and can point to completely different websites.
    In practice, it is a good idea to forward one to the other. Do this, by adding `www` or the bare domain as a redirection.

## Display

### Label

`Label` is the text that is displayed for the app on the dashboard below the icon.

### Tags

`Tags` are a mechanism to tag apps with labels. For example, you can mark specific apps with the customer name
and filter apps by customer name.

<center>
<img src="/img/apps-labels-and-tags.png" class="shadow" width="600px">
</center>

### Icon

A custom icon for the app can be set in the `Icon` section. When not set, the App package's icon is used.

## Access Control

### Access Restriction

Many apps in Cloudron are integrated with Cloudron's user management. For such
apps, one or more groups or users can be assigned to an app to restrict
login. For apps not integrated with Cloudron user management, see the section
on controlling the [visibility of app icon in dashboard](#dashboard-visibility).

Note that Cloudron only handles authentication. Assigning roles to users is
done within the application itself. For example, changing a user to become a
`commenter` or `author` inside WordPress has to be done within WordPress.

<center>
<img src="/img/apps-configure-group-acl.png" class="shadow" width="600px">
</center>

* `Allow all users from this Cloudron` - Any user in the Cloudron can access the app.
* `Only allow the following users and groups` - Only the users and groups can access the app.

### Dashboard Visibility

The Dashboard of a Cloudron user displays the apps that the user can access. For apps that
use Cloudron Single Sign-on, the dashboard only displays an app if the user
[has access to it](#access-restriction).

For apps configured to not use the Cloudron Single Sign-on (for example, some public app like a
Forum or Chat), the apps are displayed (by default) on the dashboard of all users. Admins
can control if an app appears in a user's dashboard using the `Dashboard Visibility` section
in the app's configure UI.

<center>
<img src="/img/apps-dashboard-visibility.png" class="shadow" width="600px">
</center>

### Operators

An admin can set user(s) & group(s) as the operators of an app. An app operator can perform configuration
and maintanence tasks. Unlike an app admin, an operator cannot uninstall the app or change it's location.
Operators cannot clone apps either because they do not have the permissions to install new apps.

<center>
<img src="/img/apps-operators.png" class="shadow" width="600px">
</center>

An operator will see the gear icon on their dashboard:

<center>
<img src="/img/apps-operator-button.png" class="shadow" width="250px">
</center>

On clicking the gear icon, they will see the operator UI:

<center>
<img src="/img/apps-operator-view.png" class="shadow" width="600px">
</center>

## Info

Various app information can be found in the `Info` section of the app:

<center>
<img src="/img/apps-info.png" class="shadow" width="600px">
</center>

* `App Title and Version` - This is the app's title and the upstream app version
* `App ID` - Unique ID of the app instance
* `Package Version` - Version of the Cloudron package. This is distinct from the (upstream) App Version
* `Installed At` - When the app was installed
* `Last Updated` - When the app was last updated

### Admin Notes

App specific notes can be saved in markdown format. Notes are shared by admins. All Admins and [App Operators](#operators)
can view and edit them.

<center>
<img src="/img/apps-admin-notes.png" class="shadow" width="600px">
</center>

### Checklist

Checklists provide a mechanism to inform administrators of urgent and security related tasks that need to be carried out
at the earliest. Examples include changing the default admin credentials, reviewing registration settings, etc.

Checklist appears in the `Info` section of the app.

<center>
<img src="/img/apps-checklist.png" class="shadow" width="600px">
</center>

When a Checklist item is marked as done, the username and date of completion is tracked for audit purposes.

<center>
<img src="/img/apps-checklist-done.png" class="shadow" width="600px">
</center>

## Resources

### Memory limit

All apps are run with a memory limit to ensure that no app can bring down the whole
Cloudron. The default memory limit of an app is set by the app author at packaging
time. This limit is usually the minimum amount of RAM required for the app.
Cloudron admins are expected to tweak the memory limit of an app based on their usage.

When an app runs out of memory, Cloudron automatically restarts it and sends an OOM
notification to Cloudron admins.

The memory limit can be set by adjusting the slider in the `Resources` section
of the app's configure view.

<center>
<img src="/img/apps-memory-slider.png" class="shadow" width="600px">
</center>

!!! note "Unlimited Swap"
    The memory limit specified above is just the RAM. All apps get unlimited swap.

### Low Resource Warning

When you try to install a new app, a 'Low Resource Warning' message may be displayed
based on the calculation of maximum memory limits of existing installed apps. This is a warning
that the server will run out of memory, in case all apps are close to their set memory limit.

The warning is shown based on a conservative estimate, because more often than not, apps use
well below their maximum memory limit.

<center>
<img src="/img/apps-low-resource-warning.png" class="shadow" width="600px">
</center>

### CPU Quota

By default, all apps use as much CPU as they need. To constrain the maximum CPU usage, you
can set a CPU Quota. If your server has 16 cores, then a setting of 50%, will limit the app
to use a maximum of 8 cores at a time.

The CPU quota can be set by adjusting the slider in the `Resources` section
of the app's configure view.

<center>
<img src="/img/apps-cpu-quota.png" class="shadow" width="600px">
</center>

## Storage

### Data Directory

Apps store their data and assets in the `/home/yellowtent/appsdata/<appid>` directory. If the
server is running out of disk space (in the root filesystem), you can move the app's storage
directory to another location. In most cases, this is an external disk mounted on the server. For example,
you can mount a DigitalOcean [Block Storage](https://www.digitalocean.com/docs/volumes/) or AWS [Block Store](https://aws.amazon.com/ebs/)
and move the app's data to that disk.

For example, to move an app's data to an external disk location like `/mnt/seagate`:

* Add the external disk as a [volume](/volumes/#add) named `seagate`.

* Go to the app's `Storage` section and select the volume. An optional prefix may be specifed to store the data in a subdirectory.

<center>
<img src="/img/apps-data-directory.png" class="shadow" width="600px">
</center>

!!! warning "Volume Type"
    `cifs` and `sshfs` volumes are unsuitable as an app's data directory as they do not support file permissions and ownership.
    `mountpoint` and `nfs` volumes may or may not work depending on the destination filesystem.

!!! note "App Data Directory is backed up"
    The external app data directory is part of the app's backup.

### Mounts

Apps on Cloudron are containerized and do not have access to the server's file system. To
provide an app access to a path on the server, one can create a [Volume](/storage/#volumes) and then mount the
volume into the app. Apps can access any mounted volumes via `/media/{volume name}` directory in their file system.

For example, to give an app access to an external disk `/mnt/music`:

* Create a [volume](/volumes/#add) in the `Volumes` view name `music`.

* Add an app mount.

<center>
<img src="/img/apps-mount.png" class="shadow" width="600px">
</center>

The app can access the music files from `/media/music` (which corresponds to the host path `/mnt/songs`).

When the read only flag is checked, the `/media/music` directory is not writable.

!!! note "Mounts are not backed up"
    Volumes are not backed up. Restoring an app will not restore the volume's content. Please make sure to have a suitable backup plan if you write to them.

## Email

### Mail FROM address

For apps that can send email, Cloudron automatically assigns an address of the
form `<location>.app`. To change this name, go to the `Email` section in the app's configure UI.

<center>
<img src="/img/apps-mailbox-name.png" class="shadow" width="600px">
</center>

!!! note "Display name"
    Support for email address display name depends on the app. If the display name input box is missing,
    it means that the app doesn't support it (possibly because it uses a dynamic display name). If the
    display name is empty, the app package provides a suitable default (usually the app's title).

### Disable Email Configuration

For select apps, you can also disable email auto-configuration using `Do not configure app's mail delivery settings`.
When selected, Cloudron will not configure email delivery settings inside the app, you can set it
up yourself.

<center>
<img src="/img/apps-mailbox-disable.png" class="shadow" width="600px">
</center>

!!! note "This is not a mailbox, just an address"
    The app is simply configured to send mails with the above name. If you want to receive
    email with the address, be sure to [create a mailbox](/email/#add).
    If a mailbox with the name does not exist, any replies to the email will bounce.

### Inbox

For apps that can receive email, the inbox address for the app can be assigned in the `Email` section
of the app's configure UI.

<center>
<img src="/img/apps-inbox.png" class="shadow" width="600px">
</center>

When an inbox address is assigned, Cloudron will configure the app to receive mails using that address.
It will also generate a dynamic username and password for the app to use to access the inbox.

An inbox address can only be assigned, if the email server for the domain in hosted on Cloudron. If the email
server is external to Cloudron, use the "Do not configure inbox" option and configure the app on your own.

!!! note "Mailbox must be manually created"
    The app is simply configured to receive mails with the above address. You must [create a mailbox](/email/#add)
    for emails to be received by the mail server.

## Security

### robots.txt

The `Robots.txt` file is a file served from the root of a website to indicate which parts must be indexed by a search
engine. The file follows the [Robots Exclusion Standard](https://en.wikipedia.org/wiki/Robots_exclusion_standard).
Google has an [excellent document](https://developers.google.com/search/reference/robots_txt) about the semantics.

The robots.txt contents of an app can be set in the `Security` section of the app's configure UI.

By default, Cloudron does not setup a robots.txt for apps. When unset, the app is free to provide it's own robots.txt.

<center>
<img src="/img/apps-robots-txt.png" class="shadow" width="600px">
</center>

In addition, the Cloudron admin page has a hardcoded robots.txt that disables indexing:
```
User-agent: *
Disallow: /
```

### Custom CSP

The CSP HTTP header instructs the browser to only load scripts, media, images and other resources only from specific
sites. Some apps set these headers to be overly restrictive and provide no way to customize them. For such apps,
you can override the CSP headers set by the app.

For example, to [embed Mattermost](https://github.com/mattermost/docs/blob/master/source/integrations/embedding.rst)
in another site, you can set the following CSP policy for Mattermost:

```
frame-ancestors site.example.com;
```

<center>
<img src="/img/apps-security-csp.png" class="shadow" width="600px">
</center>

### HSTS Preload

[HSTS Preload](https://hstspreload.org/) is a list of sites that are hardcoded into Chrome as being HTTPS only. Most major browsers (Chrome, Firefox, Opera, Safari, IE 11 and Edge) also have HSTS preload lists based on the Chrome list.

Requirements and implications:

* Due to the size of the preload list, automated preload list submissions of whole registered domains (bare domain) are accepted.
* This will prevent all subdomains and nested subdomains being accessed without a valid HTTPS certificate.
* New entries are hardcoded into the Chrome source code and can take several months before they reach the stable version.

When enabled, Cloudron will server the following HSTS headers:

```
Strict-Transport-Security: max-age=63072000; includeSubDomains; preload
```

To enable HSTS Preload, enable it in the `Security` section of the app:

<center>
<img src="/img/apps-security-hsts-preload.png" class="shadow" width="600px">
</center>

!!! note "Submission"
    Cloudron does not automatically submit the domain to the HSTS Preload list. You must do that manually [here](https://hstspreload.org/).

## Cron

Cron jobs required for the app to function are already integrated into the app package and no further
configuration is required. If you want to run additional custom cron commands, you can add them in the `Cron` section.

Cron commands are run with the exact same context as the app (in a one-off container). This means that they have access to all the
same environment and databases as the app itself. They also follow the life cycle states of the app. When an
app is stopped, they don't run anymore. The log output of the cron commands can be viewed using the [log viewer](#log-viewer).

Cron times are specified in UTC.

The schedule pattern can also be one of the following [cron extensions](https://www.man7.org/linux/man-pages/man5/crontab.5.html#EXTENSIONS):

* `@service`   :    Run once on app restart or if app is already running.
* `@reboot `   :    Run once on app restart or if app is already running.
* `@yearly`    :    Run once a year, ie.  `0 0 1 1 *`.
* `@annually`  :    Run once a year, ie.  `0 0 1 1 *`.
* `@monthly`   :    Run once a month, ie. `0 0 1 * *`.
* `@weekly`    :    Run once a week, ie.  `0 0 * * 0`.
* `@daily`     :    Run once a day, ie.   `0 0 * * *`.
* `@hourly`    :    Run once an hour, ie. `0 * * * *`.

<center>
<img src="/img/apps-cron.png" class="shadow" width="600px">
</center>

!!! note "Chaining commands"
    The command can be chained using `&&` or '||' . For example, `echo "=> Doing job" && /app/data/do_job.sh`

## Services

### Redis

By default, apps requiring Redis for caching are set up to use a standalone internal redis database. If you want to conserve resources or your usage of the app doesn't necessitate caching, you can disable redis in the `Services` section.

<center>
<img src="/img/apps-disable-redis.png" class="shadow" width="600px">
</center>


### TURN

By default, apps requiring STUN/TURN configuration are set up to use the built-in TURN service. If you prefer to use an external one, this can be disabled in the `Services` section.

<center>
<img src="/img/apps-disable-turn.png" class="shadow" width="600px">
</center>

## Web Terminal

Cloudron provides a web terminal that gives access to the app's file system. The web terminal
can be used to introspect and modify the app's files, access the app's database etc. Note that
Cloudron runs apps as containers with a read-only file system. Only `/run` (dynamic data),
`/app/data` (backup data) and `/tmp` (temporary files) are writable.

The web terminal can be accessed using the Web Terminal button:

<center>
<img src="/img/apps-terminal-button.png" class="shadow" width="400px">
</center>

Clicking the icon will pop up a new window. The terminal is essentially a shell into the app's file system.

<center>
<img src="/img/apps-terminal-exec2.png" class="shadow" width="600px">
</center>

## File Manager

Cloudron provides a File Manager that be used to modify the app's file system from the browser.

The File Manager can be accessed using the File Manager button:

<center>
<img src="/img/apps-filemanager-button.png" class="shadow" width="600px">
</center>

Clicking the icon will pop up a new window. Note that there are action like Rename, Delete, Change Ownership
in the context menu.

<center>
<img src="/img/filemanager.png" class="shadow" width="600px">
</center>

## SFTP Access

Certain apps like WordPress, LAMP, Surfer support access to their data via SFTP. Files can be viewed
and uploaded using any SFTP client. The FTP connection information can be displayed by clicking
the `SFTP Access` menu item.

<center>
<img src="/img/apps-sftp-info.png" class="shadow" width="600px">
</center>

A SFTP client like [FileZilla](https://filezilla-project.org/) can be used to connect as follows:

* `Host` - `sftp://my.cloudron.space` (host is the same for SFTP access to all apps)
* `Username` - `girish@lamp.cloudron.space` (username is different for SFTP access to each app)
* `Password` - Cloudron password (password is the same for SFTP access to all apps)
* `Port` - 222

<center>
<img src="/img/sftp-filezilla.png" class="shadow" width="600px">
</center>

Only Cloudron admins have SFTP access. To give a specific user access to SFTP of a single app,
make them an [operator](#operators).

!!! note "Port 222"
    SFTP service runs at port 222. The server firewall already has this port open. However, you will
    have to whitelist this port in the Cloud firewall (e.g EC2 Security Group or DigitalOcean Firewall).
    If the domain is fronted by Cloudflare, use the IP address of the server to connect via SFTP instead
    of `my.domain.com`.

## Log Viewer

To view the logs of an app, click the logs button:

<center>
<img src="/img/apps-logs-button.png" class="shadow" width="600px">
</center>

This will open up a popup dialog that display the logs:

<center>
<img src="/img/apps-logs.png" class="shadow" width="600px">
</center>

Up to 10MB of current logs and one rotated log is retained per app. Logs older than 14 days are removed. The raw logs are located at `/home/yellowtent/platformdata/logs/<appid>/`.

## Graphs

The Graphs view shows an overview of the CPU, disk, network and memory usage of the app.

<center>
<img src="/img/apps-graphs-memory.png" class="shadow" width="600px">
</center>

<center>
<img src="/img/apps-graphs-cpu.png" class="shadow" width="600px">
</center>

<center>
<img src="/img/apps-graphs-diskio.png" class="shadow" width="600px">
</center>

<center>
<img src="/img/apps-graphs-networkio.png" class="shadow" width="600px">
</center>


## Stop App

An app can be stopped using the Stop button from the app toolbar.

<center>
<img src="/img/app-stop-button.png" class="shadow" width="600px">
</center>

## Uninstall

An app can be uninstalled clicking the `Uninstall` button in the app's configure UI.

<center>
<img src="/img/app-uninstall-button.png" class="shadow" width="600px">
</center>

Uninstalling an app immediately removes all data associated with the app from the Cloudron.

!!! note "Backups are not removed"
    App backups are not removed when it is uninstalled and are only cleaned up based on the backup
    policy. Apps can always be [restored](/backups/#restoring-an-app-from-existing-backup)
    from their backups using the CLI tool.

## Version

There are two independent versions associated with an app. These are shown in the [info](#info) section.

* The `Package version`. Cloudron uses [semver](https://semver.org/) for it's app packages.
* The `App version` or `Upstream version`. App version format varies wildly. It can be date based, semver, git commit based, numbers etc.

### Install Specific Version

When you install an app from the `App Store` view, it installs the latest package. It is possible
to install older packages and older versions of the app, as long as the Cloudron version supports it.

To determine the version, you have to look into the package's source code. All Cloudron app packages are
open source and available at https://git.cloudron.io/cloudron . Each app package is in it's own repository
and has the `-app` suffix.

For example, [Managed WordPress](https://git.cloudron.io/cloudron/wordpress-managed-app), [GitLab](https://git.cloudron.io/cloudron/gitlab-app),
[Espo CRM](https://git.cloudron.io/cloudron/espocrm-app) and so on.

To install a specific package version:

* First, determine the desired package version. In each git repo, there is a `CHANGELOG` . Let's say we want
  to install WordPress 6.4.3 . For this, we locate [this line](https://git.cloudron.io/cloudron/wordpress-managed-app/-/blob/master/CHANGELOG?ref_type=heads#L663).
  The line above says that Package Version 3.6.1 has WordPress 6.4.3.

* Go to `App Store` view and click on the app.

    <center>
    <img src="/img/app-install-version.png" class="shadow" width="600px">
    </center>

* Change the URL bar to `?version=3.6.1` parameter as above to the desired package version and press enter.

* Install the app

!!! note "Automatic update"
    If you want to stick to the installed version, be sure to disable [automatic app updates](/updates/#app-updates) .

