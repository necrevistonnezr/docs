# Upgrading to Ubuntu 22.04 (Jammy Jellyfish)

## Overview

In this guide, we will see how to upgrade an existing Ubuntu 20.04 based Cloudron to Ubuntu 22.04.
If you are still on Ubuntu 18, you must first upgrade to Ubuntu 20 before upgrading to Ubuntu 22.
Follow [this guide](/guides/upgrade-ubuntu-20/) to upgrade to Ubuntu 20.04.

Please note that Ubuntu 20.04 will be supported by Canonical till 2024. Cloudron will support
Ubuntu 20.04 and 22.04. It has the same feature set across both the versions.

## Checklist

Before upgrading, please note the following:

* Cloudron has to be on atleast version 7.2. This can be verified by checking the version in the Settings view. Cloudron releases prior to 7.2.0 do not support Ubuntu 22.04.
* Ubuntu has to be on version 20.04. Check the output of `lsb_release -a` to confirm this.
* The upgrade takes around 1-3 hours based on various factors like network/cpu/disk etc

!!! warning "Upgrade fails with Full text search feature"
    There is a bug in Cloudron 7.2 where in the mail server does not start in Ubuntu 22 when the
    [Full text search](/email/#full-text-search) feature is turned on. Please wait till Cloudron 7.3
    to upgrade.

## Pre-flight

Before starting the upgrade process, it's a good idea to create a server snapshot to rollback quickly. If your VPS does not have snapshotting feature, it's best to create a full Cloudron backup before attempting the upgrade (Backups -> Create Backup now).

!!! warning "Highly recommend taking server snapshot"
    Taking a server snapshot will save a lot of trouble if your server is unable to start up after a failed Ubuntu upgrade. If your VPS provider does not have snapshot feature, it is critical to have backups _outside_ of your server. This is because in corner cases Ubuntu fails to boot entirely and your data and backups will be locked up inside the server disk.

## Upgrading

Start the upgrade:

```
# dpkg --configure -a
# apt update
# apt upgrade
# do-release-upgrade
```

Upgrade notes:

* Accept running an additional ssh deamon at port 1022
* Choose 'yes' to Restart services without asking.
* For all packages (mime, nginx, timesyncd, logrotate, journald etc), select N or O  : keep your currently-installed version. This is the 'default'.
* `nginx` upgrade might fail towards the end. You can ignore this.

Once upgrade is complete, restart the server.

## Post Upgrade

* Verify that ubuntu upgraded using `lsb_release -a`.

* Run `/home/yellowtent/box/scripts/init-ubuntu.sh` to install missing packages.

* Run `/home/yellowtent/box/setup/start.sh` to fix up package configuration.

* Ubuntu 22 uses cgroups v2 by default. All the docker containers have to be recreated to recognize this change in host configuration. To complete the upgrade, run `/home/yellowtent/box/scripts/recreate-containers` script. This script is only part of Cloudron 7.3. For earlier versions, it can be downloaded [here](https://git.cloudron.io/cloudron/box/-/raw/master/scripts/recreate-containers).

## Post Upgrade checks

* `lsb_release -a` will output Ubuntu 22.04.
* `systemctl status box` will say `active (running)`.
* `systemctl status collectd` will say `active (running)`.
* Verify that all the services in the Services view of Cloudron dashboard are running.

