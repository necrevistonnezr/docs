# Upgrading to Ubuntu 24.04 (Noble Numbat)

## Overview

In this guide, we will see how to upgrade an existing Ubuntu 22.04 based Cloudron to Ubuntu 24.04.
If you are still on Ubuntu 20, you must first upgrade to Ubuntu 22 before upgrading to Ubuntu 24.
Follow [this guide](/guides/upgrade-ubuntu-22/) to upgrade to Ubuntu 22.04.

Please note that Ubuntu 22.04 will be [supported](https://wiki.ubuntu.com/Releases) by Canonical till 2027.
Cloudron will support Ubuntu 22.04 and 24.04. It has the same feature set across both the versions.

## Checklist

Before upgrading, please note the following:

* Cloudron has to be on atleast version 8.0. This can be verified by checking the version in the Settings view. Cloudron releases prior to 8.0 do not support Ubuntu 24.04.
* Ubuntu has to be on version 22.04. Check the output of `lsb_release -a` to confirm this.
* The upgrade takes around 1-3 hours based on various factors like network/cpu/disk etc

## Pre-flight

Before starting the upgrade process, it's a good idea to create a server snapshot to rollback quickly. If your VPS does not have snapshotting feature, it's best to create a full Cloudron backup before attempting the upgrade (Backups -> Create Backup now).

!!! warning "Highly recommend taking server snapshot"
    Taking a server snapshot will save a lot of trouble if your server is unable to start up after a failed Ubuntu upgrade. If your VPS provider does not have snapshot feature, it is critical to have backups _outside_ of your server. This is because in corner cases Ubuntu fails to boot entirely and your data and backups will be locked up inside the server disk.

## Upgrading

Start the upgrade:

```
# dpkg --configure -a
# apt update
# apt upgrade
# do-release-upgrade -d
```

Upgrade notes:

* Accept running an additional ssh deamon at port 1022
* Choose 'yes' to Restart services without asking.
* For all packages (mime, nginx, timesyncd, logrotate, journald etc), select N or O  : keep your currently-installed version. This is the 'default'.

Once upgrade is complete, restart the server.

## Post Upgrade

* Fixup collectd. `/etc/default/collectd` might have a `LD_PRELOAD` line from previous releases. If you make any changes, restart collectd using `systemctl restart collectd` .

```
# DELETE THIS LINE OR EQUIVALENT
# LD_PRELOAD=/usr/lib/python3.10/config-3.10-x86_64-linux-gnu/libpython3.10.so
```

## Post Upgrade checks

* `lsb_release -a` will output Ubuntu 24.04.
* `systemctl status box` will say `active (running)`.
* Verify that all the services in the Services view of Cloudron dashboard are running.
* Verify app graphs are working. If it is, it means that `collectd` is working.

