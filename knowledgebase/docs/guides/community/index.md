# Cloudron Community Guides

## Overview

The purpose of the Cloudron Community Guides section is to provide [Cloudron Community](https://forum.cloudron.io) created documentation in an effort to provide additional information on the Cloudron platform, apps, and tutorials.

!!! warning "Community Guide"
    This guide was contributed by the Cloudron Community, and does not imply support, warranty. You should utilize them at your own risk. You should be familiar with technical tasks, ensure you have backups, and throughly test.

## Contributing

Please send a [merge request](https://git.cloudron.io/cloudron/docs) to contribute to the docs here.

