# Import MongoDB

## Overview

In this guide, we will see how to export a MongoDB database from your current setup and import it into
the MongoDB database of a Cloudron app.

## Export

The first step is to create a dump in your existing MongoDB database setup:

```
$ mongodump -h host:port -u mongouser -p mongopassword --db databasename --out /tmp
$ tar -C /tmp -zcvf /tmp/mongodump.tar.gz databasename
```

If the MongoDB server is on Cloudron, you can export it using this command line on the [Web Terminal](/apps#web-terminal):

```
# mongodump -u "${CLOUDRON_MONGODB_USERNAME}" -p "${CLOUDRON_MONGODB_PASSWORD}" -h ${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT} --db ${CLOUDRON_MONGODB_DATABASE} --out /tmp

# tar -C /tmp -zcvf /tmp/mongodump.tar.gz ${CLOUDRON_MONGODB_DATABASE}
```

## Import

* After install, enable 'Recovery Mode'in the `Repair` section. This will ensure that the app is paused and not actively
  using the database while you are importing.

<center>
<img src="/guides/img/app-enable-recovery-mode.png" class="shadow" width="500px">
</center>

* Open a [Web Terminal](/apps#web-terminal) by using the Terminal button in the `Console` section.

<center>
<img src="/guides/img/app-terminal.png" class="shadow" width="500px">
</center>

* In the terminal, upload the dump file using the `Upload to /tmp` button.

<center>
<img src="/guides/img/app-upload.png" class="shadow" width="500px">
</center>

* Extract the dump:

```
root@e6bb147e-9f9c-4d17-a9af-65d9fbb0dd72:/tmp# tar -zxvf mongodump.tar.gz
e6bb147e-9f9c-4d17-a9af-65d9fbb0dd72/
e6bb147e-9f9c-4d17-a9af-65d9fbb0dd72/rocketchat_custom_sounds.metadata.json
e6bb147e-9f9c-4d17-a9af-65d9fbb0dd72/users.metadata.json
e6bb147e-9f9c-4d17-a9af-65d9fbb0dd72/rocketchat_custom_sounds.bson
...
```

* Clear the existing database:

```
# mongosh -u "${CLOUDRON_MONGODB_USERNAME}" -p "${CLOUDRON_MONGODB_PASSWORD}" ${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT}/${CLOUDRON_MONGODB_DATABASE} --eval 'db.getCollectionNames().forEach(function (col) { db.getCollection(col).deleteMany({}) })'
```

* In the terminal, import the dump using the `mongorestore` command.

```
# mongorestore -u "${CLOUDRON_MONGODB_USERNAME}" -p "${CLOUDRON_MONGODB_PASSWORD}" -h ${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT} --db ${CLOUDRON_MONGODB_DATABASE} --dir=/tmp/e6bb147e-9f9c-4d17-a9af-65d9fbb0dd72/
```

* To start the app again, click the `Disable Recovery Mode` in `Repair` section:

<center>
<img src="/guides/img/app-disable-recovery-mode.png" class="shadow" width="500px">
</center>


## Verify

Click the `MongoDB` button on top of the terminal to paste the command line required to access the MongoDB database.
You can now press enter/return to get the MongoDB shell.

<center>
<img src="/guides/img/mongodb-shell.png" class="shadow" width="500px">
</center>

