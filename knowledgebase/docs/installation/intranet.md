# Intranet

## Prerequisites

Cloudron can be installed in an intranet as long as the following prerequisites are met. For this
document, intranet is defined as a private network behind a corporate firewall. The server uses private
IPs and users connect to the server after connecting to the corporate network with a VPN.

### IPv4 / IPv6

By default, Cloudron uses the public IPv4 / IPv6 for configuring the DNS. For intranet setups, this will most likely
be incorrect.

In the [Domain Setup UI](/installation/#domain-setup), click on `Advanced Settings` and choose `Network Interface`
or the `Static IP` provider. Be sure to configure the Cloudron VM to have this static internal IP address across reboot
(usually in your DHCP server).

### DNS Provider

Cloudron supports a variety of [DNS providers](/domains/#dns-providers) to automatically configure the DNS.
When using one of the programmatic providers, Cloudron can get Let's Encrypt certificates using DNS automation.

In an intranet setup, Cloudron has no way to get Let's Encrypt certificates without a programmatic DNS provider.
If you are unable to choose a programmatic DNS provider during installation time, choose `Self-signed` certificates
in the `Advanced Settings` of the [Domain Setup UI](/installation/#domain-setup). Later, you can upload
[valid certificates](/certificates/#custom-certificates) in the `Domains` view or better yet switch to a
supported DNS provider to get Let's Encrypt certificates.

<center>
<img src="/img/installation-intranet.png" class="shadow" width="500px">
</center>

!!! warning "Self-signed certificates"
    We discourage use of Cloudron with self-signed certificates. The issue is not of security but of usability.
    Most mobile apps do not work with self-signed certificates. Users keep seeing nagging scary screens on
    their browsers and the overall user experience is poor.


