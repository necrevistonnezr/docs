# Install

## Install

Run the following commands on a fresh Ubuntu Jammy 24.04 (x64) server:

```
wget https://cloudron.io/cloudron-setup
chmod +x cloudron-setup
./cloudron-setup
```

You can find referral links to get started on various cloud providers with free credits [here](https://cloudron.io/get.html).

!!! note "Minimum Requirements"
    Cloudron requires at least 2GB RAM, 20GB Disk space. Make sure the firewall does not block port 80 (http)
    and 443 (https). Cloudron does not support running on ARM, LXC, Docker or OpenVZ (Open Virtuozzo).

!!! tip "Marketplace"
    Cloudron is available pre-installed as on various marketplaces - [AWS](https://aws.amazon.com/marketplace/pp/B082Q3MNBR),
    [DigitalOcean](https://marketplace.digitalocean.com/apps/cloudron), [Hostinger](https://support.hostinger.com/en/articles/8637788-how-to-use-the-ubuntu-22-04-template-with-cloudron), [Linode](https://www.linode.com/marketplace/apps/cloudron/cloudron/), [Time4VPS](https://community.time4vps.com/discussion/541/cloudron-getting-started-with-cloudron) and [Vultr](https://www.vultr.com/marketplace/apps/cloudron/).

!!! tip "Network requirements"
    Depending on your network, please read the [Home Server](/installation/home-server) or the [Intranet](/installation/intranet)
    installation considerations.

## Setup

Once installation is complete, navigate to `https://<IP>` in your browser and accept the self-signed
certificate.

In Chrome, you can accept the self-signed certificate by clicking on `Advanced` and then
click `Proceed to <ip> (unsafe)`. In Firefox, click on `Advanced`, then `Accept the Risk and Continue`.

<center>
<img src="/img/installation-setup.png" class="shadow" width="500px">
</center>

## Domain Setup

Provide a domain like `example.com`.  The way Cloudron works is that the dashboard gets installed at
 `my.example.com`, and apps are installed under subdomains that you specify like `git.example.com`,
`chat.example.com`, and so on.

It is perfectly safe to use a domain that is already in use as long as the `my` subdomain is available.
When installing apps, Cloudron will never overwrite your existing DNS records and your existing subdomains will
remain intact. It is also possible to use a subdomain like `cloudron.example.com`.

<center>
<img src="/img/installation-setupdns.png" class="shadow" width="500px">
</center>

Next, select the DNS service in which the domain in hosted. If your service is not listed below, use the
`Wildcard` or `Manual` option. See [DNS Providers](/domains/#dns-providers) for the various providers and options.

!!! note "Primary domain"
    The first domain added on Cloudron is called the `Primary Domain`. The dashboard is made available
    under the `my` subdomain of the primary domain. More domains can be added after installation in the
    in the [Domains view](/domains). The Primary Domain can be changed post installation.

## Admin Account

Once DNS is setup, Cloudron will redirect to `https://my.example.com`. The browser address bar
will show a green lock to indicate that the connection to your Cloudron is now secure.

<center>
<img src="/img/installation-admin-account.png" class="shadow" width="500px">
</center>

Enter the adminstrator username, email and password for Cloudron.

The email address is used for password resets and notifications. It is local to
your Cloudron and not sent anywhere (including cloudron.io).

!!! warning "Let's Encrypt requires a valid admin email"
    Cloudron sets up a Let's Encrypt account with the administrator's email. If this email
    address is not valid, Let's Encrypt will not issue certificates and Cloudron will fall back
    to using self-signed certs.

## App Store Account

You are now ready to start installing apps! When you click on the `App Store` link in the UI,
you will be prompted to create a [cloudron.io](https://cloudron.io) account. This account is
used to manage your subscription and billing.

<center>
<img src="/img/installation-appstore-signup.png" class="shadow" width="500px">
</center>

## Firewall Setup

Security is a core feature of Cloudron and the default installation is already setup
to follow [best practices](/security/). We do not recommend adding and modifying
rules in `iptables`/`ip6tables` since Cloudron already does this. All unneeded ports are blocked.
Ports are whitelisted as and when the apps you install require them.

To further harden security, we recommend:

* [configuring the VPS Firewall](/security/#cloud-firewall)
* [securing SSH access](/security/#securing-ssh-access)

## Email Setup

Given the proliferation of Email spam, many email providers block emails from VPS and Home network IPs.
To ensure reliable delivery of email from apps and Cloudron, check the [Email delivery](/email/#server-status) status
and possibly set up an [Email relay](/email/#relay-outbound-mails).

Note that as part of app packaging, all apps are pre-configured to use Cloudron's internal mail server.
You only need to configure the relay in the Cloudron dashboard and all apps will automatically use the relay.

