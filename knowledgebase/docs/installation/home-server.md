# Home Server

## Prerequisites

Cloudron can be installed in a home network as long as the following prerequisites are met.

### Public IPv4 / IPv6

If you require Cloudron to be accessible from outside your home, you need a public IPv4 or IPv6 address.
This IP address does not need to be static. Post installation, you can use the [Dynamic DNS](/networking/#dynamic-dns)
feature to keep your DNS automatically up-to-date. You can visit [this site](https://whatismyipaddress.com/?ref=docs.cloudron.io)
to view your current public IP address.

If you do not require Cloudron to be accessible from outside your home, click on `Advanced Settings` in
the [Domain Setup UI](/installation/#domain-setup). Then, choose `Static IP` and provide the internal IP
of your server. If you decide to do this, you **must** use a Programmatic DNS provider (see below).
With an internal IP and no programmatic DNS, Cloudron will not be able to get certificates from Let's Encrypt.

### DNS Provider

Cloudron supports a variety of [DNS providers](/domains/#dns-providers) to automatically configure the DNS.
When using one of the programmatic providers, Cloudron can get Let's Encrypt certificates using DNS automation.

If you decide not to use one of those providers and instead use `Wildcard` or `Manual` DNS, then you must also forward
port 80 from your router to the server. This is required to obtain Let's Encrypt certificates.

!!! warning "Self-signed certificates"
    We discourage use of Cloudron with self-signed certificates. The issue is not of security but of usability.
    Most mobile apps do not work with self-signed certificates. Users keep seeing nagging scary screens on
    their browsers and the overall user experience is poor.

### Port Forwarding

If you require Cloudron to be accessible from outside the home network, you must port forward 443 in your router's
firewall to the Cloudron server. See [this site](https://portforward.com/router.htm?ref=docs.cloudron.io) for router
specific instructions on how to setup port forwarding.

Some apps use custom TCP ports (for git, p2p, etc). You need to set up port forwarding for those as well when you install
the apps.

!!! warning "Port 443"
    Be sure to forward port 443 before you do the [domain setup](/installation/#domain-setup). Otherwise, you cannot
    reach the dashboard after the domain setup.

### NAT Loopback

[NAT loopback](https://en.wikipedia.org/wiki/Hairpinning?ref=docs.cloudron.io) or Hairpinning is a feature of the router allowing
internal services to access self or other services via the public IP. This feature allows an app on Cloudron to
reach another app on Cloudron using the DNS name (which resolves to the public IP). This feature is crucial for
OIDC login to work. Most modern routers support this.

