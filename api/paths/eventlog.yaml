definitions:
  Event:
    description: Event
    type: object
    properties:
      id:
        type: string
        description: Event Id
        example: "7f32f714-ad40-4bea-b5e5-e71562679f0c"
      action:
        type: string
        description: Name of an action
        enum:
          - app.clone
          - app.configure
          - app.repair
          - app.install
          - app.restore
          - app.import
          - app.uninstall
          - app.update
          - app.update.finish
          - app.backup
          - app.backup.finish
          - app.login
          - app.oom
          - app.up
          - app.down
          - app.start
          - app.stop
          - app.restart
          - backup.finish
          - backup.start
          - backup.cleanup.finish
          - certificate.new
          - certificate.cleanup
          - cloudron.activate
          - cloudron.provision
          - cloudron.install.finish
          - cloudron.start
          - dashboard.domain.update
          - domain.add
          - domain.update
          - domain.remove
          - dyndns.update
          - mail.location
          - mail.enabled
          - mail.disabled
          - mail.box.add
          - mail.box.remove
          - mail.box.update
          - mail.list.add
          - mail.list.remove
          - mail.list.update
          - service.configure
          - service.rebuild
          - service.restart
          - cloudron.update
          - cloudron.update.finish
          - user.add
          - user.login
          - user.login.ghost
          - user.logout
          - user.remove
          - user.update
          - volume.add
          - volume.update
          - volume.remount
          - volume.remove
          - support.ticket
          - support.ssh
        example: "volume.add"
      creationTime:
        type: string
        description: Time when event happenned
        example: 2022-03-05T02:30:00.000Z
      source:
        type: object
        description: Source of the event
      data:
        type: object
        description: Additional data about the event


parameters:
  eventId:
    name: eventId
    in: path
    description: Event Id
    required: true
    schema:
      type: string

/eventlog:
    get:
      operationId: getEventlog
      summary: List Events
      description: List events
      tags: [ "Eventlog" ]
      security:
        - bearer_auth: [ 'read' ]
        - query_auth: [ 'read' ]
      parameters:
        - $ref: '../parameters.yaml#/PaginationPage'
        - $ref: '../parameters.yaml#/PaginationPerPage'
        - name: search
          in: query
          description: Search string
          required: false
          schema:
            type: string
        - name: actions
          in: query
          description: Comma separated list of actions
          explode: false
          required: false
          schema:
            type: array
            items:
              $ref: '#/definitions/Event/properties/action'
            example: "volume.add,volume.remove"
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  eventlogs:
                    type: array
                    items:
                      $ref: '#/definitions/Event'
        401:
          $ref: '../responses.yaml#/unauthorized'
        403:
          $ref: '../responses.yaml#/forbidden'
        500:
          $ref: '../responses.yaml#/server_error'
      x-codeSamples:
        - lang: cURL
          source: |-
            curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/eventlog"

/eventlog/{eventId}:
  get:
      operationId: getEvent
      summary: Get Event
      description: Get event by ID
      tags: [ "Eventlog" ]
      security:
        - bearer_auth: [ 'read' ]
        - query_auth: [ 'read' ]
      parameters:
        - $ref: '#/parameters/eventId'
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/definitions/Event'
        401:
          $ref: '../responses.yaml#/unauthorized'
        403:
          $ref: '../responses.yaml#/forbidden'
        404:
          $ref: '../responses.yaml#/not_found'
        500:
          $ref: '../responses.yaml#/server_error'
      x-codeSamples:
        - lang: cURL
          source: |-
            curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/eventlog/$EVENT_ID"
