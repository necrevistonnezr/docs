# Components: For each service
components:
  schemas:
    mail:
      description: Service information for Mail service.
      type: object
      properties:
        name:
          type: string
          example: mail
        status:
          type: string
          example: active
        memoryUsed:
          type: integer
          example: 755478528
        memoryPercent:
          type: integer
          example: 17
        error:
          type: 
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          properties:
            status:
              type: boolean
              example: true
            haraka:
              type: object
              properties:
                status:
                  type: boolean
                  example: true
            dovecot:
              type: object
              properties:
                status:
                  type: boolean
                  example: true
            spamd:
              type: object
              properties:
                status:
                  type: boolean
                  example: true
            redis:
              type: object
              properties:
                status:
                  type: boolean
                  example: true
            solr:
              type: object
              properties:
                status:
                  type: boolean
                  example: true
        config:
          type: object
          example: {}
        defaultMemoryLimit:
          type: integer
          example: 536870912

    docker:
      description: Service information for Docker service.
      type: object
      properties:
        name:
          type: string
          example: docker
        status:
          type: string
          example: active
        error:
          type: 
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          example: null
        config:
          type: object
          example: {}
        defaultMemoryLimit:
          type: integer
          example: 0

    graphite:
      description: Service information for Graphite service.
      type: object
      properties:
        name:
          type: string
          example: graphite
        status:
          type: string
          example: active
        memoryUsed:
          type: integer
          example: 193470464
        memoryPercent:
          type: integer
          example: 18
        error:
          type:
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          example: null
        config:
          type: object
          example:
            memoryLimit: 1073741824
            recoveryMode: false
        defaultMemoryLimit:
          type: integer
          example: 268435456

    mongodb:
      description: Service information for MongoDB service.
      type: object
      properties:
        name:
          type: string
          example: mongodb
        status:
          type: string
          example: active
        memoryUsed:
          type: integer
          example: 363716608
        memoryPercent:
          type: integer
          example: 16
        error:
          type:
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          properties:
            status:
              type: boolean
              example: true
        config:
          type: object
          example:
            memoryLimit: 2147483648
            recoveryMode: false
        defaultMemoryLimit:
          type: integer
          example: 805306368

    mysql:
      description: Service information for MySQL service.
      type: object
      properties:
        name:
          type: string
          example: mysql
        status:
          type: string
          example: active
        memoryUsed:
          type: integer
          example: 505319424
        memoryPercent:
          type: integer
          example: 62
        error:
          type:
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          properties:
            status:
              type: boolean
              example: true
        config:
          type: object
          example:
            memoryLimit: 805306368
            recoveryMode: false
        defaultMemoryLimit:
          type: integer
          example: 805306368

    nginx:
      description: Service information for Nginx service.
      type: object
      properties:
        name:
          type: string
          example: nginx
        status:
          type: string
          example: active
        error:
          type:
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          example: null
        config:
          type: object
          example: {}
        defaultMemoryLimit:
          type: integer
          example: 0

    postgresql:
      description: Service information for PostgreSQL service.
      type: object
      properties:
        name:
          type: string
          example: postgresql
        status:
          type: string
          example: active
        memoryUsed:
          type: integer
          example: 178421760
        memoryPercent:
          type: integer
          example: 8
        error:
          type:
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          properties:
            status:
              type: boolean
              example: true
        config:
          type: object
          example:
            memoryLimit: 2147483648
            recoveryMode: false
        defaultMemoryLimit:
          type: integer
          example: 805306368

    sftp:
      description: Service information for SFTP service.
      type: object
      properties:
        name:
          type: string
          example: sftp
        status:
          type: string
          example: active
        memoryUsed:
          type: integer
          example: 53608448
        memoryPercent:
          type: integer
          example: 19
        error:
          type:
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          example: null
        config:
          type: object
          example:
            requireAdmin: true
            memoryLimit: 268435456
        defaultMemoryLimit:
          type: integer
          example: 268435456

    turn:
      description: Service information for TURN service.
      type: object
      properties:
        name:
          type: string
          example: turn
        status:
          type: string
          example: active
        memoryUsed:
          type: integer
          example: 18948096
        memoryPercent:
          type: integer
          example: 7
        error:
          type:
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          example: null
        config:
          type: object
          example:
            memoryLimit: 268435456
            recoveryMode: false
        defaultMemoryLimit:
          type: integer
          example: 268435456

    unbound:
      description: Service information for Unbound service.
      type: object
      properties:
        name:
          type: string
          example: unbound
        status:
          type: string
          example: active
        error:
          type: 
            - string
            - 'null'
          example: null
        healthcheck:
          type: object
          example: null
        config:
          type: object
          example: {}
        defaultMemoryLimit:
          type: integer
          example: 0

parameters:
  service:
    name: service
    in: path
    description: Service name
    required: true 
    schema:
      type: string
  fromMinutes:
    name: fromMinutes
    in: query
    description: Time range for getting graphs of a service.
    required: true
    schema:
      type: number

/services:
  get:
    operationId: getServices
    summary: List services
    description: List services
    tags: [ "Services" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              properties:
                services:
                  type: array
                  example: ["turn","mail","mongodb","mysql","postgresql","docker","unbound","sftp","graphite","nginx"]
                  items:
                    description: service
                    type: string
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services"

/services/platform_status:
  get:
    operationId: getPlatformStatus
    summary: Get platform status
    description: Get platform status
    tags: [ "Services" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              properties:
                message:
                  type: object
                  example: "Ready"
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services"

/services/{service}:
  get:
    operationId: getService
    summary: Get service
    description: Get information about a service.
    tags: [ "Services" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/service'
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              anyOf:
                  - $ref: '#/components/schemas/mail'
                  - $ref: '#/components/schemas/docker'
                  - $ref: '#/components/schemas/graphite'
                  - $ref: '#/components/schemas/mongodb'
                  - $ref: '#/components/schemas/mysql'
                  - $ref: '#/components/schemas/nginx'
                  - $ref: '#/components/schemas/postgresql'
                  - $ref: '#/components/schemas/sftp'
                  - $ref: '#/components/schemas/turn'
                  - $ref: '#/components/schemas/unbound'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services/$SERVICE"
  post:
    operationId: configureService
    summary: Configure service
    description: This call is used to configure a service.
    tags: [ "Services" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/service'
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              memoryLimit:
                type: number
                description: Limits the available memory. The unit used here is bytes.
                example: 4294967296
              recoveryMode:
                type: boolean
                description: Enables or disables the recovery mode.
                example: false
            required:
              - memoryLimit
              - recoveryMode
    responses:
      202:
        $ref: '../responses.yaml#/accepted'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services/$SERVICE" -- data '{"memoryLimit":4294967296, "recoveryMode": false}'

/services/{service}/graphs:
  get:
    operationId: getServiceGraphs
    summary: Get service graphs
    description: Get graphs of the service
    tags: [ "Services" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/service'
      - name: fromMinutes
        in: query
        description: Filter by minutes.
        required: true
        schema:
          type: number
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                cpu:
                  type: array
                memory:
                  type: array
                blockRead:
                  type: array
                blockWrite:
                  type: array
                networkRead:
                  type: array
                networkWrite:
                  type: array
                blockReadTotal:
                  type: number
                blockWriteTotal:
                  type: number
                networkReadTotal:
                  type: number
                networkWriteTotal:
                  type: number
                cpuCount:
                  type: number
                  example: 4
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services/$SERVICE/graphs?fromMinutes=10"

/services/{service}/logs:
  get:
    operationId: downloadServiceLogs
    summary: Download service logs
    description: Download logs of a service.
    tags: [ "Services" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - $ref: '#/parameters/service'
      - name: lines
        in: query
        description: Last lines displayed.
        required: true
        schema:
          type: number
      - name: format
        in: query
        description: Defines the format of the logs.
        required: false
        schema:
          type: string
          enum: [json, short]
          default: json
    responses:
      200:
        description: Success
        content:
          application/x-logs:
            schema:
              type: string
              format: binary
              description: >
                Each line in the returned logs is of the desired format. When the format is `json`, each line is a [NDJSON](https://en.wikipedia.org/wiki/JSON_streaming#Newline-Delimited_JSON)
                containing the fields `realtimeTimestamp` and `message`. The `Content-Disposition` header is set to `attachment; filename="mail.log"`.
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -v \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services/$SERVICE/logs/?lines=10&format=json"

/services/{service}/logstream:
  get:
      operationId: geServiceLogstream
      summary: Get Service logstream
      description: >
        Get Service logs. Use this endpoint to stream logs (`tail -f` style). If you want to download logs, use the [logs](#operation/getServiceLogs) endpoint instead.
        This endpoint implements [Server Side Events](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events). Use the
        [EventSource](https://developer.mozilla.org/en-US/docs/Web/API/EventSource) interface to parse the returned stream.
      tags: [ "Services" ]
      security:
        - bearer_auth: [ 'read' ]
        - query_auth: [ 'read' ]
      parameters:
        - $ref: '#/parameters/service'
        - name: lines
          in: query
          description: Last lines displayed.
          required: true
          schema:
            type: number
        - name: format
          in: query
          description: Defines the format of the logs.
          required: false
          schema:
            type: string
            enum: [json, short]
            default: json
      responses:
        200:
          description: Success
          content:
            text/event-stream:
              schema:
                $ref: '../definitions.yaml#/LogStreamSSE'
        401:
          $ref: '../responses.yaml#/unauthorized'
        403:
          $ref: '../responses.yaml#/forbidden'
        404:
          $ref: '../responses.yaml#/not_found'
        500:
          $ref: '../responses.yaml#/server_error'
      x-codeSamples:
        - lang: cURL
          source: |-
            curl \
            -H "Accept: text/event-stream" \
            -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services/$SERVICE/logstream?lines=100"

/services/{service}/restart:
  post:
    operationId: restartService
    summary: Restart service
    description: Restart a service.
    tags: [ "Services" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/service'
    responses:
      202:
        $ref: '../responses.yaml#/accepted'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services/$SERVICE/restart"

/services/{service}/rebuild:
  post:
    operationId: rebuildService
    summary: Rebuild service
    description: Rebuild a service.
    tags: [ "Services" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    parameters:
      - $ref: '#/parameters/service'
    responses:
      202:
        $ref: '../responses.yaml#/accepted'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/services/$SERVICE/rebuild"