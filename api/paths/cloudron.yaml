/cloudron/status:
  get:
    operationId: getStatus
    summary: Get Status
    description: Simple healthcheck route to check if Cloudron is running or not. This route does not require any authentication.
    tags: [ "Cloudron" ]
    security: []
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                version:
                  type: string
                  description: The current Cloudron Version
                  example: "7.6.0"
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" "https://$CLOUDRON_DOMAIN/api/v1/cloudron/status"

/cloudron/avatar:
  get:
    operationId: getAvatar
    summary: Get Avatar (icon)
    description: |
      Get the Cloudron Avatar. The Cloudron avatar (icon) is used in Email templates, Dashboard header and the Login pages.
      This route does not require any authentication.
    tags: [ "Cloudron" ]
    security: []
    responses:
      200:
        description: Success
        content:
          image/png:
            schema:
              type: string
              format: binary
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" "https://$CLOUDRON_DOMAIN/api/v1/cloudron/avatar"

/cloudron/background:
  get:
    operationId: getCloudronBackground
    summary: Get Cloudron background
    description: |
      Get the Cloudron Background.
    tags: [ "Cloudron" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          image/png:
            schema:
              type: string
              format: binary
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" "https://$CLOUDRON_DOMAIN/api/v1/cloudron/background"

/cloudron/languages:
  get:
    operationId: getLanguages
    summary: Get Languages
    description: List the available languages (translations). This route does not require any authentication.
    tags: [ "Cloudron" ]
    security: []
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                languages:
                  type: array
                  description: An array containing the Language IDs
                  items:
                    type: string
                    description: Translation name
                  example:
                    - en
                    - de
                    - fr
                    - nl
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" "https://$CLOUDRON_DOMAIN/api/v1/cloudron/languages"

/cloudron/language:
  get:
    operationId: getCloudronLanguage
    summary: Get Cloudron Language
    description: >-
      The Cloudron Language is the language used for the Dashboard, Login Page, Invitation and Reset emails. Note that users can
      always set a different language for their Dashboard in their profile. The default language is `en` (English).
    tags: [ "Cloudron" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                language:
                  type: string
                  description: The Language ID
                  default: en
                  example: de
      404:
        $ref: '../responses.yaml#/not_found'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/cloudron/language"

  post:
    operationId: setCloudronLanguage
    summary: Set Cloudron Language
    description: >
        The Cloudron Language is the language used for the Dashboard, Login Page, Invitation and Reset emails. Note that users can
        always set a different language for their Dashboard in their profile. The default language is `en` (English).
    tags: [ "Cloudron" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              language:
                  type: string
                  description: The Language ID
                  default: en
                  example: de
            required:
              - language
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/cloudron/language" --data '{"language":"de"}'

/cloudron/time_zone:
  get:
    operationId: getCloudronTimeZone
    summary: Get Cloudron Time Zone
    description: >-
      TimeZone used for various cron jobs like backup, updates, date display in emails etc. Note that server time zone and database is always UTC.
    tags: [ "Cloudron" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                timeZone:
                  type: string
                  description: The [Time Zone Identifier](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
                  default: UTC
                  example: de
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/cloudron/time_zone"

  post:
    operationId: setCloudronTimeZone
    summary: Set Cloudron Time Zone
    description: >
      TimeZone used for various cron jobs like backup, updates, date display in emails etc. Note that server time zone and database is always UTC.
    tags: [ "Cloudron" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              timeZone:
                  type: string
                  description: The [Time Zone Identifier](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
                  default: UTC
                  example: Europe/Zurich
            required:
              - language
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/cloudron/time_zone" --data '{"timeZone":"Africa/Lome"}'
