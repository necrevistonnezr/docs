definitions:
  info:
    type: object
    properties:
      sysVendor:
        type: string
        description: The vendor of the system.
        example: "ACME Inc."
      productName:
        type: string
        description: The name of the product.
        example: "Server"
      uptimeSecs:
        type: integer
        description: The number of seconds since the system was last restarted.
        example: 3600
      rebootRequired:
        type: boolean
        description: Indicates whether a system reboot is required.
        example: false
      activationTime:
        type: string
        description: The date and time when the product was activated.
        example: "2024-05-15T08:00:00Z"

  cpus:
    type: array
    example: [{ "model": "AMD EPYC Processor","speed": 2445, "times": { "user": 43406350, "nice": 595090, "sys": 23041560, "idle": 1932436310, "irq": 0 }},{"model": "AMD EPYC Processor", "speed": 2445, "times": { "user": 43047330, "nice": 605510, "sys": 22909070, "idle": 1932424940, "irq": 0}}]
    items:
      type: object
      properties:
        model:
          type: string
          example: AMD EPYC Processor
          description: The model name of the CPU.
        speed:
          type: integer
          example: 2555
          description: The speed of the CPU in MHz.
        times:
          type: object
          description: Object containing CPU times (in milliseconds).
          properties:
            user:
              type: integer
              example: 234023
              description: Time spent in user mode.
            nice:
              type: integer
              example: 321123
              description: Time spent in nice mode.
            sys:
              type: integer
              example: 3214321
              description: Time spent in system mode.
            idle:
              type: integer
              example: 3214432
              description: Time spent in idle mode.
            irq:
              type: integer
              example: 0
              description: Time spent in IRQ mode.

  devices:
    type: array
    items:
      type: object
      properties:
        path:
          type: string
          description: The device file path.
          example: "/dev/sda1"
        size:
          type: string
          description: The size of the device.
          example: "40G"
        type:
          type: string
          description: The filesystem type of the device.
          example: "ext4"
        uuid:
          type: string
          description: The UUID of the device.
          example: "55347856adabef0-1957-463e-534543534asdc"
        mountpoint:
          type: string
          description: The mount point of the device.
          example: "/"

/system/info:
  get:
    operationId: getInfo
    summary: Get system info 
    description: >
      Get system information.
    tags: [ "System" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              $ref: '#/definitions/info'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/info"

/system/reboot:
  post:
    operationId: rebootServer
    summary: Reboot
    description: Reboots the server immediately.
    tags: [ "System" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    responses:
      202:
        $ref: '../responses.yaml#/no_content'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/reboot"

/system/cpus:
  get:
    operationId: getCpus
    summary: Get cpus 
    description: >
      Get information of your sytem cpus.
    tags: [ "System" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
            application/json:
              schema:
                type: object
                properties:
                  cpus:
                    $ref: '#/definitions/cpus'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/cpus"

/system/disk_usage:
  get:
    operationId: getDiskUsage
    summary: Get Disk Usage
    description: Get information about disk usage for apps and other system components, like mail storage and docker images.
    tags: [ "System" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                usage:
                  type: object
                  properties:
                    ts:
                      type: integer
                      description: Timestamp when info was last updated in milliseconds since the Unix Epoch
                      example: 1699412641120
                    disks:
                      type: object
                      properties:
                        /dev/nvme0n1p2:
                          type: object
                          description: This property is a unique disk identifier based on the linux device path.
                          properties:
                            available:
                              type: integer
                              description: Remaining free disk space in bytes
                              example: 190863687680
                            capacity:
                              type: number
                              description: Fractional number of percentage the disk is in use.
                              example: 0.6
                            contents:
                              type: array
                              description: An array containing all units this disk holds, apps, volumes, built-in units like email
                              items:
                                type: object
                                properties:
                                  id:
                                    type: string
                                    example: platformdata
                                    description: usage unit identifier could be appId or built-in types
                                  path:
                                    type: string
                                    example: /home/yellowtent/platformdata
                                  type:
                                    type: string
                                    description: unit type
                                    example: standard
                                  usage:
                                    type: integer
                                    description: Disk space used by this unit in bytes
                                    example: 2969190400
                            filesystem:
                              type: string
                              example: /dev/nvme0n1p2
                            mountpoint:
                              type: string
                              example: /
                            size:
                              type: integer
                              example: 502391562240
                            speed:
                              type: number
                              example: 523.6
                            type:
                              type: string
                              example: ext4
                            used:
                              type: integer
                              example: 285932556288
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/disk_usage"
  post:
    operationId: updateDiskUsage
    summary: Update Disk Usage
    description: Disk usage is only updated automatically once per day. This API allows to trigger an update of that information.
    tags: [ "System" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    responses:
      202:
        $ref: '../responses.yaml#/task_started'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/disk_usage"

/system/block_devices:
  get:
    operationId: getBlockDevices
    summary: Get block devices 
    description: >
      Get information of your sytem block devices.
    tags: [ "System" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
            application/json:
              schema:
                type: object
                properties:
                  devices:
                    $ref: '#/definitions/devices'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/block_devices"

/system/memory:
  get:
    operationId: getMemory
    summary: Get memory 
    description: >
      Get information of your sytems memory.
    tags: [ "System" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
            application/json:
              schema:
                type: object
                properties:
                  memory:
                    type: integer
                    example: 32423423
                  swap:
                    type: integer
                    example: 32123442
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/memory"

/system/logs/{unit}:
  get:
    operationId: downloadSystemLogs
    summary: Download system logs
    description: Download the system logs.
    tags: [ "System" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    parameters:
      - name: unit
        in: path
        description: Unit for the logs.
        required: true
        schema:
          type: number
      - name: lines
        in: query
        description: Last lines displayed.
        required: false
        schema:
          type: number
      - name: format
        in: query
        description: Defines the format of the logs.
        required: false
        schema:
          type: string
          enum: [json, short]
          default: json
    responses:
      200:
        description: Success
        content:
          application/x-logs:
            schema:
              type: string
              format: binary
              description: >
                Each line in the returned logs is of the desired format. When the format is `json`, each line is a [NDJSON](https://en.wikipedia.org/wiki/JSON_streaming#Newline-Delimited_JSON)
                containing the fields `realtimeTimestamp` and `message`. The `Content-Disposition` header is set to `attachment; filename="box.log"`.
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -v \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/logs/$UNIT/?lines=10&format=json"

/system/logstream/{unit}:
  get:
      operationId: getLogStream
      summary: Get logstream
      description: >
        Get system logs. Use this endpoint to stream logs (`tail -f` style). If you want to download logs, use the [logs](#operation/getLogs) endpoint instead.
        This endpoint implements [Server Side Events](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events). Use the
        [EventSource](https://developer.mozilla.org/en-US/docs/Web/API/EventSource) interface to parse the returned stream.
      tags: [ "System" ]
      security:
        - bearer_auth: [ 'read' ]
        - query_auth: [ 'read' ]
      parameters:
        - name: unit
          in: path
          description: Unit for the logs.
          required: true
          schema:
            type: number
        - name: lines
          in: query
          description: Last lines displayed.
          required: false
          schema:
            type: number
        - name: format
          in: query
          description: Defines the format of the logs.
          required: false
          schema:
            type: string
            enum: [json, short]
            default: json
      responses:
        200:
          description: Success
          content:
            text/event-stream:
              schema:
                $ref: '../definitions.yaml#/LogStreamSSE'
        401:
          $ref: '../responses.yaml#/unauthorized'
        403:
          $ref: '../responses.yaml#/forbidden'
        404:
          $ref: '../responses.yaml#/not_found'
        500:
          $ref: '../responses.yaml#/server_error'
      x-codeSamples:
        - lang: cURL
          source: |-
            curl \
            -H "Accept: text/event-stream" \
            -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/system/logstream/$UNIT?lines=100"