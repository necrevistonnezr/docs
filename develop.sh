#!/bin/bash

set -eu

export MKDOCS_MATERIAL_VERSION="9.5.27"
export MKDOCS_REDIRECTS_VERSION="1.2.1"

echo "=> Installing mkdocs-material"
python3 -m venv ./.python-venv
./.python-venv/bin/pip install mkdocs-material==${MKDOCS_MATERIAL_VERSION} mkdocs-redirects==${MKDOCS_REDIRECTS_VERSION} pillow cairosvg

echo "=> Installing @redocly/cli"
npm install -g @redocly/cli

cd knowledgebase
../.python-venv/bin/mkdocs serve
